package com.lohika.training.bigdata.imagecompare.library.function.map;

import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

import com.lohika.training.bigdata.imagecompare.library.type.Image;
import com.lohika.training.bigdata.imagecompare.library.type.StoredInterestPoint;

public class ImagePointsToMatchFunction implements PairFunction<Tuple2<Image, StoredInterestPoint>, Image, Integer> {

    static final int NEGATIVE_SAFETY_BIT_MASK = 0x20080200;

    @Override
    public Tuple2<Image, Integer> call(final Tuple2<Image, StoredInterestPoint> pair) {
        /*boolean pointMatch = checkPointMatch(pair._2.getDescriptor(), pair._2.isSign()
                ? pair._1.getPlusSignInterestPoints() : pair._1.getMinusSignInterestPoints());
        return new Tuple2<Image, Integer>(pair._1, Integer.valueOf(pointMatch ? 1 : 0));*/
        return null;
    }

    /*public static boolean checkPointMatch(int[] v1, int[][] ipts) {
        int bestDistance = Integer.MAX_VALUE;
        int secondBest = Integer.MAX_VALUE;
        int i, delta, delta1, delta2, delta3, distance = 0;

        ipts2Loop:
        for (int[] v2 : ipts) {

            // Compare descriptors (based on calculating of squared distance between two vectors)
            distance = 0;
            for (i = 0; i < 21; i++) {
                delta = (v1[i] | NEGATIVE_SAFETY_BIT_MASK) - v2[i];

                delta1 = delta << 23 >> 23;
                delta2 = delta << 13 >> 23;
                delta3 = delta << 3 >> 23;

                distance += (delta1 * delta1) + (delta2 * delta2) + (delta3 * delta3);

                if (distance >= secondBest) {
                    continue ipts2Loop;
                }
            }
            delta1 = (v1[21] | NEGATIVE_SAFETY_BIT_MASK) - v2[21];
            distance += (delta1 * delta1);

            if (distance < bestDistance) {
                secondBest = bestDistance;
                bestDistance = distance;
            }
            else  {
                secondBest = distance;
            }

        }

        // Threshold values in other implementations:
        // OpenSURF:                    0.65
        // OpenCV-2.0.0 (find_obj.cpp): 0.6
        // Orig. SURF:                  0.5

        // If min distance is less than 1/2 second min distance
        return bestDistance < 0.5f * secondBest;
    }*/
}
