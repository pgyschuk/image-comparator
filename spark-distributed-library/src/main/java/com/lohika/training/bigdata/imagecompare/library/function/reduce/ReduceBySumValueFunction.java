package com.lohika.training.bigdata.imagecompare.library.function.reduce;

import org.apache.spark.api.java.function.Function2;

public class ReduceBySumValueFunction  implements Function2<Integer, Integer, Integer> {

    @Override
    public Integer call(Integer x, Integer y) throws Exception {
        return x + y;
    }

}
