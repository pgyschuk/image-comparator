package com.lohika.training.bigdata.imagecompare.library.function.filter;

import java.util.List;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

public class FilterImagesByPath implements Function<Row, Boolean> {

    final List<String> pathsToCheck;

    public FilterImagesByPath(List<String> pathsToCheck) {
        this.pathsToCheck = pathsToCheck;
    }

    @Override
    public Boolean call(Row row) throws Exception {
        return pathsToCheck.contains(row.getString(1));
    }
}
