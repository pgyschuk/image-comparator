package com.lohika.training.bigdata.imagecompare.library.type;

import java.io.Serializable;
import java.util.List;

import com.clearspring.analytics.util.Lists;

public class Image implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 113740125659927529L;

    String path;
    int[][] plusSignInterestPoints;
    int[][] minusSignInterestPoints;
    long timestamp = System.currentTimeMillis();

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int[][] getPlusSignInterestPoints() {
        return plusSignInterestPoints;
    }

    public void setPlusSignInterestPoints(int[][] plusSignInterestPoints) {
        this.plusSignInterestPoints = plusSignInterestPoints;
    }

    public int[][] getMinusSignInterestPoints() {
        return minusSignInterestPoints;
    }

    public void setMinusSignInterestPoints(int[][] minusSignInterestPoints) {
        this.minusSignInterestPoints = minusSignInterestPoints;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public static void setStoredPois(Image image, List<StoredInterestPoint> storedPOIs) {
        List<int[]> plusList = Lists.newArrayList();
        List<int[]> minusList = Lists.newArrayList();

        for (StoredInterestPoint p : storedPOIs) {
            if (p.sign) {
                plusList.add(p.descriptor);
            } else {
                minusList.add(p.descriptor);
            }
        }
        int[][] plusSignInterestPoints = new int[plusList.size()][];
        int[][] minusSignInterestPoints = new int[minusList.size()][];
        plusList.toArray(plusSignInterestPoints);
        minusList.toArray(minusSignInterestPoints);
        image.setPlusSignInterestPoints(plusSignInterestPoints);
        image.setMinusSignInterestPoints(minusSignInterestPoints);
    }
}
