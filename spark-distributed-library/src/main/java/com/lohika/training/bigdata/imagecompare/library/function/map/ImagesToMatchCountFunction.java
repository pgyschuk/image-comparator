package com.lohika.training.bigdata.imagecompare.library.function.map;

import java.util.Map;

import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

import com.google.common.collect.Maps;
import com.lohika.training.bigdata.imagecompare.library.type.Image;

public class ImagesToMatchCountFunction implements PairFunction<Tuple2<Image, Image>, Image, Integer> {

    @Override
    public Tuple2<Image, Integer> call(final Tuple2<Image, Image> pair) {
        int matchCount = countMathes(pair._2.getPlusSignInterestPoints(), pair._1.getPlusSignInterestPoints())
                + countMathes(pair._2.getMinusSignInterestPoints(), pair._1.getMinusSignInterestPoints());
        return new Tuple2<Image, Integer>(pair._1, matchCount);
    }

    public int countMathes(int[][] ipts1, int[][] ipts2) {
        Map<Integer, Integer> matchedPoints = findMathes(ipts1, ipts2);
        Map<Integer, Integer> matchedPointsReverse = findMathes(ipts2, ipts1);

        // count only those points that matched in the reverse comparison too
        int count = 0;
        for (Map.Entry<Integer, Integer> match : matchedPoints.entrySet()) {
            if (match.getKey().equals(matchedPointsReverse.get(match.getValue()))) {
                count++;
            }
        }
        return count;
    }

    public Map<Integer, Integer> findMathes(int[][] ipts1, int[][] ipts2) {
        Map<Integer, Integer> matches = Maps.newHashMap();

        int distance, bestDistance, secondBest, bestMatch = 0;
        int i, ipts1Index, ipts2Index, delta1, delta2, delta3;
        int val1, val2;

        for (ipts1Index = 0; ipts1Index < ipts1.length; ipts1Index++) {
            bestDistance = secondBest = Integer.MAX_VALUE;

            ipts2Loop:
            for (ipts2Index = 0; ipts2Index < ipts2.length; ipts2Index++) {

                // Compare descriptors (based on calculating of squared distance between two vectors)
                distance = 0;
                for (i = 0; i < 21; i++) {
                    val1 = ipts1[ipts1Index][i];
                    val2 = ipts2[ipts2Index][i];

                    delta1 = (val1 << 22 >> 22) - (val2 << 22 >> 22);
                    delta2 = (val1 << 12 >> 22) - (val2 << 12 >> 22);
                    delta3 = (val1 << 2 >> 22) - (val2 << 2 >> 22);

                    distance += (delta1 * delta1) + (delta2 * delta2) + (delta3 * delta3);

                    if (distance >= secondBest) {
                        continue ipts2Loop;
                    }
                }
                delta1 = ipts1[ipts1Index][21] - ipts2[ipts2Index][21];
                distance += (delta1 * delta1);

                if (distance < bestDistance) {
                    secondBest = bestDistance;
                    bestDistance = distance;
                    bestMatch = ipts2Index;
                }
                else {//if (distance < secondBest) {
                    secondBest = distance;
                }

            }

            // Threshold values in other implementations:
            // OpenSURF:                    0.65
            // OpenCV-2.0.0 (find_obj.cpp): 0.6
            // Orig. SURF:                  0.5
            if (bestDistance < 0.5f * secondBest) {
                // Matching point found.
                matches.put(ipts1Index, bestMatch);
            }
        }

        return matches;
    }
}