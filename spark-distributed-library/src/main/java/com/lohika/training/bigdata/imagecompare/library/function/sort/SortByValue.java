package com.lohika.training.bigdata.imagecompare.library.function.sort;

import org.apache.spark.api.java.function.Function;

import scala.Tuple2;

public class SortByValue implements Function<Tuple2<String, Integer>, Integer> {

    @Override
    public Integer call(Tuple2<String, Integer> val) throws Exception {
        return val._2;
    }
}
