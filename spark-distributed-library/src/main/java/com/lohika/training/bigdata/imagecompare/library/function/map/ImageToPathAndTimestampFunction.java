package com.lohika.training.bigdata.imagecompare.library.function.map;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

import scala.Tuple2;

public class ImageToPathAndTimestampFunction implements Function<Row, Tuple2<String, Long>>{

    @Override
    public Tuple2<String, Long> call(Row row) throws Exception {
        return new Tuple2<String, Long>(row.getString(1), row.getLong(3));
    }
}
