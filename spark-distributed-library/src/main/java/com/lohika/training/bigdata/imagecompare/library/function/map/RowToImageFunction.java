package com.lohika.training.bigdata.imagecompare.library.function.map;

import java.util.List;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Row;

import scala.collection.mutable.WrappedArray;

import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import com.lohika.training.bigdata.imagecompare.library.type.Image;

// TODO optimize
public class RowToImageFunction implements Function<Row, Image> {

	@Override
    public Image call(Row row) {
        Image image = new Image();
        image.setPath(row.getString(1));

        List<WrappedArray<Integer>> wrappedData = row.getList(2);
        image.setPlusSignInterestPoints(readIntArray(wrappedData));

        wrappedData = row.getList(0);
        image.setMinusSignInterestPoints(readIntArray(wrappedData));

        image.setTimestamp(row.getLong(3));

        return image;
    }

    private int[][] readIntArray(List<WrappedArray<Integer>> data) {
        int[][] pois = new int[data.size()][];
        for (int i = 0; i < data.size(); i++) {
            WrappedArray<Integer> wrappedArray = data.get(i);
            List<Integer> arr = Lists.newArrayListWithCapacity(wrappedArray.size());
            for (scala.collection.Iterator<Integer> iterator = wrappedArray.iterator(); iterator.hasNext();) {
                arr.add(iterator.next());
            }
            pois[i] = Ints.toArray(arr);
        }
        return pois;
    }
}
