package com.lohika.training.bigdata.imagecompare.library.type;

import java.io.Serializable;
import java.util.Arrays;

public class StoredInterestPoint implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1750688676204990821L;

    /**
     * Sign of hessian traces (laplacian sign).<br> <code>true</code> means >= 0, <code>false</code> means < 0. (Signs are saved separately for better matching
     * performance.)
     */
    boolean sign;

    /**
     * Vector of descriptor components.
     */
    int[] descriptor;

    public boolean isSign() {
        return sign;
    }

    public void setSign(boolean sign) {
        this.sign = sign;
    }

    public int[] getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(int[] descriptor) {
        this.descriptor = descriptor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StoredInterestPoint that = (StoredInterestPoint) o;

        if (sign != that.sign) {
            return false;
        }
        if (!Arrays.equals(descriptor, that.descriptor)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = descriptor.hashCode();
        result = 31 * result + (sign ? 1 : 0);
        return result;
    }
}
