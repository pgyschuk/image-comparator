package com.lohika.training.bigdata.imagecompare.library.function.filter;

import org.apache.spark.api.java.function.Function;

import com.lohika.training.bigdata.imagecompare.library.type.Image;

import scala.Tuple2;

public class FilterImagesByMatchCount implements Function<Tuple2<Image, Integer>, Boolean> {

    final int minCount;

    public FilterImagesByMatchCount(int minCount) {
        this.minCount = minCount;
    }

    @Override
    public Boolean call(Tuple2<Image, Integer> val) throws Exception {
        return val._2 >= minCount;
    }
}
