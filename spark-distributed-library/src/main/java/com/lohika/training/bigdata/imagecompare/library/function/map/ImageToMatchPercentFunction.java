package com.lohika.training.bigdata.imagecompare.library.function.map;

import org.apache.spark.api.java.function.Function;

import scala.Tuple2;

import com.lohika.training.bigdata.imagecompare.library.type.Image;

public class ImageToMatchPercentFunction implements Function<Tuple2<Image, Integer>, Tuple2<String, Integer>>{

    @Override
    public Tuple2<String, Integer> call(Tuple2<Image, Integer> val) throws Exception {
        int size = val._1.getPlusSignInterestPoints().length + val._1.getMinusSignInterestPoints().length;
        int percent = (int) Math.ceil(((float)val._2 / size) * 100);
        return new Tuple2<String, Integer>(val._1.getPath(), percent);
    }
}
