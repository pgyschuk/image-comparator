// --------------- ImageBrowserController ------------------

angular.module('ImageComparator', [])
  .controller('ImageBrowserController', function($scope, $http, $timeout) {

    $scope.onImgLoad = function(event) {
        if ($scope['imgCounter'] > 1) {
            $scope['imgCounter'] = $scope['imgCounter'] - 1;
        } else {
            // if all images finished loading initialize hovers
            fixImageHoverHeight();
            initJQCustom();
        }
    }

    $scope.browse = function(itemId) {
        var data = {
            itemId:itemId
        };
        var config = {
            params: data
        };
        $http.get('/browse', config).success(function(data) {
            $scope.images = data.images;
            $scope.currentPath = data.path;
            $scope.totalCount = '(' + data.totalCount + ')';

            initImgCounter(data.images, $scope);

            //$timeout(initJQCustom, 0);
        })
    }

    $scope.compareImg = function(imageId) {
        angular.element(document.getElementById('CompareImagesController')).scope().compareImg(imageId);
    }

    $scope.browse();
});

// --------------- RecentImagesController ------------------

angular.module('ImageComparator')
    .controller('RecentImagesController', function($scope, $http, $timeout) {

        $scope.onImgLoad = function(event) {
            if ($scope['imgCounter'] > 1) {
                $scope['imgCounter'] = $scope['imgCounter'] - 1;
            } else {
                // if all images finished loading initialize hovers
                fixImageHoverHeight();
                initJQCustom();
            }
        }

        $scope.browse = function() {
            $http.get('/recentimages').success(function(data) {
                $scope.images = data.images;
                if ($scope.images.length > 0) {
                    initImgCounter(data.images, $scope);
                    //$('#collapseBrowse').collapse();
                }
                //$timeout(initJQCustom, 0);
            })
        }

        $scope.compareImg = function(imageId) {
            angular.element(document.getElementById('CompareImagesController')).scope().compareImg(imageId);
        }

        $scope.browse();
});

//--------------- CompareImagesController ------------------

angular.module('ImageComparator')
    .controller('CompareImagesController', function($scope, $http, $timeout, $interval) {

        $scope.onImgLoad = function(event) {
            // TODO fix issue with wrong hover positions on second search request
            /*if ($scope['imgCounter'] > 1) {
                $scope['imgCounter'] = $scope['imgCounter'] - 1;
            } else {*/
                // if all images finished loading initialize hovers
                fixImageHoverHeight();
                initJQCustom();
            /*}*/
        }

        $scope.compareImg = function(imageId) {
            var data = {
                imageId:imageId
            };
            var config = {
                params: data
            };

            showOnscreenProgress();

            $http.get('/compareimages', config).success(function() {

                var interval = $interval(function() {
                    $http.get('/compareimages-progress').success(function(data) {
                        if (!data.progress) {
                            $interval.cancel(interval);

                            $scope.images = data.images;
                            $scope.targetImage = data.targetImage;
                            $scope.searchDone = true;
                            if ($scope.images.length > 0) {
                                initImgCounter(data.images, $scope);
                                $('#collapseBrowse').collapse();
                                $('#collapseRecent').collapse();
                            }
                            hideOnscreenProgress();
                        } else {
                            $scope.progress = data.progress ;
                            trackOnscreenProgress($scope.progress);
                        }
                    })
                }, 500);

                //$timeout(initJQCustom, 0);
            })
        }

});

// --------------------------------------------

angular.module('ImageComparator')
    .directive('sbLoad', ['$parse', function ($parse) {
      return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
          var fn = $parse(attrs.sbLoad);
          elem.on('load', function (event) {
            scope.$apply(function() {
              fn(scope, { $event: event });
            });
          });
        }
      };
}]);

function submitImageUploadForm() {
    document.getElementById('imageUploadLabel').style.display = 'none';
    document.getElementById('loadingIcon').style.display = 'block';
    document.getElementById('imageUploadForm').submit();
}

function initImgCounter(images, $scope) {
    var imgCounter = 0;
    var index;
    if (images) {
        for (index = 0; index < images.length; ++index) {
            if (!images[index].directory) {
                ++imgCounter;
            }
        }
    }
    $scope['imgCounter'] = imgCounter;
}

function fixImageHoverHeight() {
    var imgs = $('img[id^="image_"]');
    for (index = 0; index < imgs.length; ++index) {
        var imgId = imgs[index].id;
        var spanId = "iconsContainer_" + imgId.substring("image_".length, imgId.length);
        var imgHeight = imgs[index].clientHeight;
        document.getElementById(spanId).setAttribute("style","height:" + imgHeight + "px");
    }
}

$('div.accordion-body').on('shown', function () {
    $(this).parent("div").find(".icon-chevron-left")
           .removeClass("icon-chevron-left").addClass("icon-chevron-down");
});

$('div.accordion-body').on('hidden', function () {
    $(this).parent("div").find(".icon-chevron-down")
           .removeClass("icon-chevron-down").addClass("icon-chevron-left");
});

//--------------- BatchUploadController ------------------

angular.module('ImageComparator')
.controller('BatchUploadController', function($scope, $http, $interval) {

    $scope.startRefresh = function() {
        $scope.inProgress = true;
        $scope.totalImages = false;
        $scope.progress = "0";

        $http.post('/batchupload').success(function() {
            var interval = $interval(function() {
                $http.get('/batchupload-progress').success(function(data) {
                    if (!data.progress) {

                        if ($scope.progress != "100") {
                            $scope.progress = "100";
                            // make sure that progress bar shows 100% completion
                            return;
                        }

                        $interval.cancel(interval);
                        $scope.totalAdded = data.totalAdded;
                        $scope.totalImages = data.totalImages;
                        $scope.inProgress = false;
                    } else {
                        $scope.progress = data.progress;
                        $scope.inProgress = true;
                    }
                })
            }, 600);
        })

    }
});
//--------------- BatchComparisonController ------------------

angular.module('ImageComparator')
.controller('BatchComparisonController', function($scope, $http, $interval) {

    $scope.startComparison = function() {
        $("#availableForComparisonCount").hide();
        $scope.inProgress = true;
        $scope.totalImages = false;
        $scope.progress = "0";

        $http.post('/batchcomparison').success(function() {
            var interval = $interval(function() {
                $http.get('/batchcomparison-progress').success(function(data) {
                    if (!data.progress) {

                        if ($scope.progress != "100") {
                            $scope.progress = "100";
                            // make sure that progress bar shows 100% completion
                            return;
                        }

                        $interval.cancel(interval);
                        $scope.totalProcessed = data.totalProcessed;
                        $scope.totalImages = data.totalImages;
                        $scope.errorsCount = data.errorsCount;
                        $scope.inProgress = false;
                    } else {
                        $scope.progress = data.progress;
                        $scope.inProgress = true;
                    }
                })
            }, 600);
        })

    }

    $scope.stopComparison = function() {
        $http.post('/batchcomparison-stop').success(function() {
        })
    }

    $scope.startNodeGrouping = function() {

        showOnscreenProgress();

        $http.get('/nodegrouping').success(function() {

            var interval = $interval(function() {
                $http.get('/nodegrouping-progress').success(function(data) {
                    if (!data.progress) {
                        $interval.cancel(interval);
                        $scope.numberOfGroups = data.numberOfGroups;
                        $scope.numberOfMainNodes = data.numberOfMainNodes;
                        hideOnscreenProgress();
                    } else {
                        $scope.progress = data.progress;
                        trackOnscreenProgress($scope.progress);
                    }
                })
            }, 500);

        })
    }
});
// ---------------------------------------------

function onToggleShowInterestPoints() {
    var img = document.getElementById('fullResImage');
    if (img.src.indexOf("showPois=true") != -1) {
        img.src = img.src.replace("showPois=true", "");
        // remove the '&' or '?'
        img.src = img.src.substring(0, img.src.length - 1)
    } else {
        if (img.src.indexOf('?') != -1) {
            img.src = img.src + "&showPois=true";
        } else {
            img.src = img.src + "?showPois=true";
        }
    }
}

function uncheckShowInterestPoints() {
    document.getElementById('showInterestPointsToggle').checked = false;
}

function showOnscreenProgress() {
    var docHeight = $(document).height();
    $("#onscreenOverlay")
       .height(docHeight);
    trackOnscreenProgress(0);

    $("#onscreenProgress").show();
}

function hideOnscreenProgress() {
    $("#onscreenProgress").hide();
}

function trackOnscreenProgress(progress) {
    $("#onscreenText").html(progress + "%");
}