package com.lohika.training.bigdata.imagecompare;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.lohika.training.bigdata.imagecompare.model.DirectoryItem;
import com.lohika.training.bigdata.imagecompare.repositories.jpa.DirectoryItemRepository;

@Controller
public class IndexController {

    final Logger logger = LoggerFactory.getLogger(IndexController.class);

    static final String TMP_PATH = "/tmp/";
    static final String USER_HOME = System.getProperty("user.home");
    static final String ROOT_IMAGE_FOLDER = USER_HOME + "/Projects/training/images";

    @Autowired
    ImageBrowser imageBrowser;
    @Autowired
    DirectoryItemRepository directoryItemRepository;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    String index(Model model) {
        return "index2";
    }

    @RequestMapping(path = "/", method = RequestMethod.POST)
    String uploadImage(@RequestParam("file") MultipartFile[] files, Model model) throws IllegalStateException, IOException {

        for (MultipartFile file : files) {
            addNewImage(file);
        }

        return "index2";
    }

    void addNewImage(MultipartFile file) throws IllegalStateException, IOException {
        String tmpFilePath = TMP_PATH + file.getOriginalFilename();
        file.transferTo(new File(tmpFilePath));

        DirectoryItem item = directoryItemRepository.getByPath(tmpFilePath);
        if (item == null) {
            item = new DirectoryItem();
            item.setPath(tmpFilePath);
            item.setNew(true);
            directoryItemRepository.save(item);
        }
    }
}

