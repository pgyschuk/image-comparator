package com.lohika.training.bigdata.imagecompare.repositories.jpa;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.lohika.training.bigdata.imagecompare.model.DirectoryItem;

public interface DirectoryItemRepository extends CrudRepository<DirectoryItem, Long> {

    DirectoryItem getByPath(String path);

    List<DirectoryItem> findByIsNewTrue();
}
