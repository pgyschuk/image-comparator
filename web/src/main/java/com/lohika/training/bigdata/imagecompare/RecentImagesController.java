package com.lohika.training.bigdata.imagecompare;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Maps;
import com.lohika.training.bigdata.imagecompare.repositories.jpa.DirectoryItemRepository;

@RestController
public class RecentImagesController {

    @Autowired
    DirectoryItemRepository directoryItemRepository;

    @RequestMapping("/recentimages")
    public Map<String,Object> getRecentImages() {
        Map<String,Object> model = Maps.newHashMap();
        model.put("images", directoryItemRepository.findByIsNewTrue());
        return model;
    }
}
