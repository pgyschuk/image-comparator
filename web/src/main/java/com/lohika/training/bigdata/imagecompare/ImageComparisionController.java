package com.lohika.training.bigdata.imagecompare;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.concurrent.ConcurrentException;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.neo4j.core.GraphDatabase;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import scala.Tuple2;

import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lohika.training.bigdata.imagecompare.library.function.filter.FilterImagesByMatchCount;
import com.lohika.training.bigdata.imagecompare.library.function.map.ImageToMatchPercentFunction;
import com.lohika.training.bigdata.imagecompare.library.function.map.ImagesToMatchCountFunction;
import com.lohika.training.bigdata.imagecompare.library.function.sort.SortByValue;
import com.lohika.training.bigdata.imagecompare.library.type.Image;
import com.lohika.training.bigdata.imagecompare.model.ComparisonResult;
import com.lohika.training.bigdata.imagecompare.model.DirectoryItem;
import com.lohika.training.bigdata.imagecompare.model.ImageNode;
import com.lohika.training.bigdata.imagecompare.repositories.jpa.DirectoryItemRepository;
import com.lohika.training.bigdata.imagecompare.repositories.neo4j.ImageNodeRepository;
import com.lohika.training.bigdata.imagecompare.spark.SparkContextHolder;
import com.lohika.training.bigdata.imagecompare.spark.SparkContextHolder.SparkListener;
import com.lohika.training.bigdata.imagecompare.util.FileUtils;
import com.lohika.training.bigdata.imagecompare.util.ImageConverter;
import com.lohika.training.bigdata.imagecompare.util.ParquetUtils;

@RestController
public class ImageComparisionController {

    final Logger logger = LoggerFactory.getLogger(ImageComparisionController.class);

    @Value("${path.images_root}")
    private String imagePath;
    private String rootImagePath;
    @Value("${path.interest_points_file}")
    String interestPointsFile;
    private String interestPointsFilePath;
    @Value("${images.comparison.min_ip_match_count}")
    Integer minIPMatchCount;

    @Autowired
    DirectoryItemRepository directoryItemRepository;
    @Autowired
    ImageNodeRepository imageNodeRepository;
    @Autowired
    Neo4jTemplate neo4jTemplate;
    @Autowired
    private GraphDatabase graphDatabase;

    @Autowired
    SparkContextHolder sparkContextHolder;

    SimpleAsyncTaskExecutor executor;
    ComparisionTask currentTask = new ComparisionTask();

    @PostConstruct
    void init() {
        this.rootImagePath = System.getProperty("user.home") + imagePath;
        this.interestPointsFilePath = System.getProperty("user.home") + interestPointsFile;
        executor = new SimpleAsyncTaskExecutor();
        executor.setConcurrencyLimit(1);
    }

    @RequestMapping("/compareimages")
    public Map<String,Object> findSimilarImages(@RequestParam Long imageId) throws Exception {
        Map<String,Object> model = Maps.newHashMap();

        sparkContextHolder.getSparkListener().resetProgress(6);

        currentTask = new ComparisionTask(imageId);
        executor.execute(currentTask);

        return model;
    }

    @RequestMapping("/compareimages-progress")
    @ResponseBody
    public Map<String,Object> findSimilarImagesProgress() throws ConcurrentException {
        Map<String,Object> model = Maps.newHashMap();

        SparkListener sparkListener = sparkContextHolder.getSparkListener();
        if (currentTask.isReady()) {
            model.put("images", currentTask.getMatchingImages());
            model.put("targetImage", currentTask.getTargetImage());
        } else {
            model.put("progress", sparkListener.getProgress());
        }

        return model;
    }

    private List<DirectoryItem> findSimilarImages(Image targetImage, Stopwatch timer) throws Exception {

        logger.info("Spark contex created {}", timer);

        // List<StoredInterestPoint> currentImagePoints = Image.getStoredPois(targetImage);

        List<DirectoryItem> matchingImages = Lists.newArrayList();

        if (new File(interestPointsFilePath).exists()) {

            // get paths of main nodes
            List<String> mainNodesPaths = Lists.newArrayList();

            try (Transaction tx = graphDatabase.beginTx()) {
                Iterable<ImageNode> mainNodes = imageNodeRepository.getMainNodes();
                for (ImageNode node : mainNodes) {
                    mainNodesPaths.add(node.getPath());
                }
            }

            if (mainNodesPaths.isEmpty()) {
                logger.warn("Need to run neo4j node grouping first");
            } else {
                //JavaRDD<Image> allImagesRDD = ParquetUtils.read(interestPointsFilePath, sparkContextHolder.get());
                JavaRDD<Image> allImagesRDD = ParquetUtils.readByPaths(interestPointsFilePath, sparkContextHolder.get(), mainNodesPaths);

                JavaRDD<Image> currentImageRDD = sparkContextHolder.get().parallelize(ImmutableList.of(targetImage));
                JavaPairRDD<Image, Image> pairRDD = allImagesRDD.cartesian(currentImageRDD);

                Tuple2<Image,Integer> matchingImage = pairRDD
                        .mapToPair(new ImagesToMatchCountFunction())
                        .filter(new FilterImagesByMatchCount(minIPMatchCount))
                        .first();

                // get all images from matching group and create new rdd
                List<String> paths = Lists.newArrayList();
                try (Transaction tx = graphDatabase.beginTx()) {
                    ImageNode mainNode = imageNodeRepository.findByPath(matchingImage._1.getPath());
                    Iterable<ImageNode> mainNodes = imageNodeRepository.getByGroupId(mainNode.getGroupId());
                    for (ImageNode node : mainNodes) {
                        paths.add(node.getPath());
                    }
                }

                allImagesRDD = ParquetUtils.readByPaths(interestPointsFilePath, sparkContextHolder.get(), paths);

                /*JavaRDD<StoredInterestPoint> currentImagePointsRDD = sparkContextHolder.get().parallelize(currentImagePoints);
                JavaPairRDD<Image, StoredInterestPoint> pairRDD = allImagesRDD.cartesian(currentImagePointsRDD);

                List<Tuple2<String,Integer>> results = pairRDD
                        .mapToPair(new ImagePointsToMatchFunction())
                        .reduceByKey(new ReduceBySumValueFunction())
                        .map(new ImageToMatchPercentFunction())
                        .filter(new FilterImagesByMatchPercent(1))
                        .sortBy(new SortByPercent(), false, 1)
                        .collect();*/

                currentImageRDD = sparkContextHolder.get().parallelize(ImmutableList.of(targetImage));
                pairRDD = allImagesRDD.cartesian(currentImageRDD);

                List<Tuple2<String,Integer>> results = pairRDD
                        .mapToPair(new ImagesToMatchCountFunction())
                        .filter(new FilterImagesByMatchCount(minIPMatchCount))
                        .map(new ImageToMatchPercentFunction())
                        .sortBy(new SortByValue(), false, 1)
                        .collect();

                logger.info("Spark job done {}", timer);

                for (Tuple2<String, Integer> result : results) {
                    String path = result._1;
                    DirectoryItem dirItem = getDirectoryItem(path);
                    dirItem.setPercent(String.valueOf(result._2));
                    matchingImages.add(dirItem);
                }
            }
        }
        return matchingImages;
    }

    private DirectoryItem getDirectoryItem(String path) {
        DirectoryItem item = directoryItemRepository.getByPath(path);
        if (item == null) {
            item = new DirectoryItem();
            item.setPath(FileUtils.getRelativePath(path, rootImagePath));
            directoryItemRepository.save(item);
        }
        return item;
    }

    private class ComparisionTask implements Runnable {

        boolean ready = false;
        List<DirectoryItem> matchingImages;
        DirectoryItem targetImage;
        Long imageId;

        public ComparisionTask() {}

        public ComparisionTask(Long imageId) {
            this.imageId = imageId;
        }

        @Override
        public void run() {
            targetImage = directoryItemRepository.findOne(imageId);

            Stopwatch timer = new Stopwatch();
            timer.start();

            ImageConverter imageConverter = new ImageConverter(rootImagePath);

            // Check if we have precalculated comparison results in neo4j for this image
            ImageNode imageNode = imageNodeRepository.findByPath(targetImage.getPath());
            if (imageNode != null) {
                matchingImages = Lists.newArrayList();
                Map<String, Integer> matchingNodes = Maps.newHashMap();

                // get precalculated results
                try (Transaction tx = graphDatabase.beginTx()) {

                    matchingNodes.putAll(getMatchingNodes(imageNode, null));

                    // also get matching images for connected nodes to improve search results relevance
//                    for (ComparisonResult comparisonResult : imageNode.getComparisonResults()) {
//                        ImageNode otherImg = neo4jTemplate.fetch(comparisonResult.getOtherImg());
//                        if (otherImg.getPath().equals(imageNode.getPath())) {
//                            otherImg = neo4jTemplate.fetch(comparisonResult.getThisImg());
//                        }
//                        matchingNodes.putAll(getMatchingNodes(otherImg, imageNode.getPath()));
//                    }

                    for (String path : matchingNodes.keySet()) {
                        DirectoryItem dirItem = getDirectoryItem(path);
                        Integer matchPercent = matchingNodes.get(path);
                        dirItem.setPercent(String.valueOf(matchPercent));
                        matchingImages.add(dirItem);
                    }

                    Collections.sort(matchingImages, (o1, o2) -> Integer.valueOf(o2.getPercent()).compareTo(Integer.valueOf(o1.getPercent())));
                }

            } else {
                // run real time comparison
                File targetFile = new File(FileUtils.getAbsolutePath(targetImage.getPath(), rootImagePath));
                try {
                    matchingImages = findSimilarImages(imageConverter.toImage(targetFile), timer);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            logger.info("All done {}", timer);
            ready = true;
        }

        private Map<String, Integer> getMatchingNodes(ImageNode imageNode, String initialNodePath) {
            Map<String, Integer> nodes = Maps.newHashMapWithExpectedSize(imageNode.getComparisonResults().size());
            for (ComparisonResult comparisonResult : imageNode.getComparisonResults()) {
                ImageNode otherImg = neo4jTemplate.fetch(comparisonResult.getOtherImg());
                if (otherImg.getPath().equals(imageNode.getPath())) {
                    otherImg = neo4jTemplate.fetch(comparisonResult.getThisImg());
                }
                if (!otherImg.getPath().equals(initialNodePath)) {
                    nodes.put(otherImg.getPath(), comparisonResult.getMatchPercent());
                }
            }
            return nodes;
        }

        public List<DirectoryItem> getMatchingImages() {
            return matchingImages;
        }

        public DirectoryItem getTargetImage() {
            return targetImage;
        }

        public boolean isReady() {
            return ready;
        }
    }
}
