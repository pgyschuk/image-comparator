package com.lohika.training.bigdata.imagecompare.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BitUtils {

    final static Logger logger = LoggerFactory.getLogger(BitUtils.class);

    public static int[] pack3IntArray512(int[] arr) {
        int[] result = new int[arr.length / 3 + 1];
        for (int i = 0; i < arr.length - 1; i+=3) {
            result[i / 3] = (arr[i] & 0x3FF) | ((arr[i + 1] & 0x3FF) << 10) | ((arr[i + 2] & 0x3FF) << 20);

            /*int val1 = (result[i / 3] << 22 >> 22);
            int val2 = (result[i / 3] << 12 >> 22);
            int val3 = (result[i / 3] << 2 >> 22);

            if (val1 != arr[i] || val2 != arr[i + 1] || val3 != arr[i + 2]) {
                throw new IllegalAccessError();
            }*/
        }
        result[21] = arr[63];
        return result;
    }

    public static int[] floatToIntArray(float[] fa) {
        int[] arr = new int[fa.length];
        for (int i = 0; i < fa.length; i++) {
            // convert to integer and make sure that value is within [-512,512] range
            arr[i] = Math.round(fa[i] * 500);
            if (arr[i] < -511 || arr[i] >= 512) {
                logger.warn("Descriptor value out of bounds " + arr[i]);
            }
        }
        return arr;
    }
}
