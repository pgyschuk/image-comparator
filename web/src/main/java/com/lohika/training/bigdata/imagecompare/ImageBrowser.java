package com.lohika.training.bigdata.imagecompare;

import java.io.File;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.clearspring.analytics.util.Lists;
import com.google.common.base.Joiner;
import com.lohika.training.bigdata.imagecompare.model.DirectoryItem;
import com.lohika.training.bigdata.imagecompare.repositories.jpa.DirectoryItemRepository;
import com.lohika.training.bigdata.imagecompare.util.FileUtils;

@Component
public class ImageBrowser {

    @Value("${path.images_root}")
    private String imagePath;
    private String rootImagePath;
    @Value("${imagedirectory.previews_count}")
    private int imageDirectoryPreviewsCount;

    @PostConstruct
    void init() {
        this.rootImagePath = System.getProperty("user.home") + imagePath;
    }

    @Autowired
    DirectoryItemRepository directoryItemRepository;

    public List<DirectoryItem> browseDir(String dirPath) {
        File folder = new File(dirPath);
        if (!folder.exists()) {
            throw new IllegalStateException("Directory " + dirPath
                    + " doesn't exist!");
        }

        DirectoryItem folderDir = directoryItemRepository.getByPath(FileUtils.getRelativePath(dirPath, rootImagePath));
        if (folderDir == null) {
            folderDir = createDirectoryItem(folder);
        }

        List<DirectoryItem> results = Lists.newArrayList();

        // check if need to add "Up" arrow to browse the parent folder
        if (!rootImagePath.equals(dirPath)) {
            String parentPath = dirPath.substring(0, dirPath.lastIndexOf(System.getProperty("file.separator")));
            DirectoryItem parentDir = directoryItemRepository.getByPath(FileUtils.getRelativePath(parentPath, rootImagePath));

            DirectoryItem upArrow = new DirectoryItem();
            upArrow.setId(parentDir.getId());
            upArrow.setDirectory(true);

            results.add(upArrow);
        }

        File[] files = folder.listFiles();
        for (File file : files) {
            DirectoryItem item = directoryItemRepository.getByPath(FileUtils.getRelativePath(file, rootImagePath));
            if (item == null) {
                item = createDirectoryItem(file);
            }
            results.add(item);
        }
        return results;
    }


    private List<Long> findPreviews(String path, int count) {
        List<Long> results = Lists.newArrayList();

        File[] files = new File(path).listFiles();
        for (File file : files) {
            if (results.size() >= count) {
                break;
            }
            if (!file.isDirectory()) {
                DirectoryItem item = directoryItemRepository.getByPath(FileUtils.getRelativePath(file, rootImagePath));
                if (item == null) {
                    item = createDirectoryItem(file);
                }
                results.add(item.getId());
            }
        }

        // recursively iterate over nested directories to find more previews
        for (File file : files) {
            if (results.size() >= count) {
                break;
            }
            if (file.isDirectory()) {
                results.addAll(findPreviews(file.getAbsolutePath(), count - results.size()));
            }
        }

        return results;
    }

    private DirectoryItem createDirectoryItem(File file) {
        DirectoryItem item = new DirectoryItem();
        item.setPath(FileUtils.getRelativePath(file, rootImagePath));
        if (file.isDirectory()) {
            item.setDirectory(true);
            List<Long> previews = findPreviews(file.getAbsolutePath(), imageDirectoryPreviewsCount);
            item.setPreviews(Joiner.on(",").join(previews));
        }
        directoryItemRepository.save(item);
        return item;
    }
}
