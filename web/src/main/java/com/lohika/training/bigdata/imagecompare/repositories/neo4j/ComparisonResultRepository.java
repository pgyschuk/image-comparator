package com.lohika.training.bigdata.imagecompare.repositories.neo4j;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.lohika.training.bigdata.imagecompare.model.ComparisonResult;

public interface ComparisonResultRepository extends GraphRepository<ComparisonResult> {

}
