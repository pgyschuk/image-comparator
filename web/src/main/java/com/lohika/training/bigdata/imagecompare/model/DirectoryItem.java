package com.lohika.training.bigdata.imagecompare.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class DirectoryItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String path;
    boolean isDirectory = false;
    String previews = "";
    boolean isNew = false;

    @Transient
    String percent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public void setDirectory(boolean isDirectory) {
        this.isDirectory = isDirectory;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public String getPreviews() {
        return previews;
    }

    public void setPreviews(String previews) {
        this.previews = previews;
    }

    @Transient
    public String getFileName() {
        return path != null ? path.substring(path.lastIndexOf(System.getProperty("file.separator")) + 1) : null;
    }
}
