package com.lohika.training.bigdata.imagecompare.model;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type="COMPARISON_RESULT")
public class ComparisonResult {
    @GraphId
    private Long relationshipId;
    @GraphProperty
    private Integer matchCount;
    @GraphProperty
    private Integer matchPercent;

    @StartNode
    private ImageNode thisImg;
    @EndNode
    private ImageNode otherImg;

    public Long getRelationshipId() {
        return relationshipId;
    }
    public void setRelationshipId(Long relationshipId) {
        this.relationshipId = relationshipId;
    }
    public Integer getMatchCount() {
        return matchCount;
    }
    public void setMatchCount(Integer matchCount) {
        this.matchCount = matchCount;
    }
    public Integer getMatchPercent() {
        return matchPercent;
    }
    public void setMatchPercent(Integer matchPercent) {
        this.matchPercent = matchPercent;
    }
    public ImageNode getThisImg() {
        return thisImg;
    }
    public void setThisImg(ImageNode thisImg) {
        this.thisImg = thisImg;
    }
    public ImageNode getOtherImg() {
        return otherImg;
    }
    public void setOtherImg(ImageNode otherImg) {
        this.otherImg = otherImg;
    }
}
