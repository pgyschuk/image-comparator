package com.lohika.training.bigdata.imagecompare.util;

import java.io.File;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;

import scala.collection.Seq;

import com.clearspring.analytics.util.Lists;
import com.lohika.training.bigdata.imagecompare.library.function.filter.FilterImagesByPath;
import com.lohika.training.bigdata.imagecompare.library.function.map.RowToImageFunction;
import com.lohika.training.bigdata.imagecompare.library.type.Image;

public class ParquetUtils {

    public static void write(List<Image> images, String fileName, JavaSparkContext jsc, SaveMode saveMode) throws Exception {

        JavaRDD<Image> imagesRDD = jsc.parallelize(images);
        final SQLContext sqlContext = new SQLContext(jsc);

        DataFrame schemaImages = sqlContext.createDataFrame(imagesRDD, Image.class);
        schemaImages.write().mode(saveMode).format("parquet").save(fileName);
    }

    public static void writeRows(JavaRDD<Image> rows, String fileName, JavaSparkContext jsc, SaveMode saveMode) throws Exception {

        final SQLContext sqlContext = new SQLContext(jsc);

        DataFrame schemaImages = sqlContext.createDataFrame(rows, Image.class);
        schemaImages.write().mode(saveMode).format("parquet").save(fileName);
    }

    public static JavaRDD<Image> read(String fileName, JavaSparkContext jsc) throws Exception {
        final SQLContext sqlContext = new SQLContext(jsc);

        DataFrame df = sqlContext.parquetFile(getParquetFileNames(fileName));
        return df.javaRDD().map(new RowToImageFunction());
    }

    public static JavaRDD<Image> read(String fileName, JavaSparkContext jsc, long date) throws Exception {
        final SQLContext sqlContext = new SQLContext(jsc);

        DataFrame df = sqlContext.parquetFile(getParquetFileNames(fileName));
        df = df.filter(df.col("timestamp").leq(date));
        return df.javaRDD().map(new RowToImageFunction());
    }

    public static JavaRDD<Row> readPaths(String fileName, JavaSparkContext jsc, long startDate) {
        final SQLContext sqlContext = new SQLContext(jsc);

        DataFrame df = sqlContext
                .parquetFile(getParquetFileNames(fileName));
        df = df.filter(df.col("timestamp").gt(startDate));
        df = df.orderBy(df.col("timestamp").asc());
        return df.toJavaRDD();
    }

    public static List<String> findNewImagePaths(String fileName, JavaSparkContext jsc, List<String> pathsToCheck) {
        final SQLContext sqlContext = new SQLContext(jsc);

        Seq<String> fileNames = getParquetFileNames(fileName);
        if (fileNames.isEmpty()) {
            // all images are new
            return pathsToCheck;
        }
        DataFrame df = sqlContext.parquetFile(fileNames);
        List<Row> rows = df.javaRDD().filter(new FilterImagesByPath(pathsToCheck)).collect();

        List<String> results = Lists.newArrayList(pathsToCheck);
        for (Row row : rows) {
            results.remove(row.getString(1));
        }
        return results;
    }

    public static JavaRDD<Image> readByPaths(String fileName, JavaSparkContext jsc, List<String> paths) {
        final SQLContext sqlContext = new SQLContext(jsc);

        DataFrame df = sqlContext.parquetFile(getParquetFileNames(fileName));
        return df.javaRDD().filter(new FilterImagesByPath(paths)).map(new RowToImageFunction());
    }

    public static long getImageCount(String fileName, JavaSparkContext jsc) throws Exception {
        final SQLContext sqlContext = new SQLContext(jsc);

        Seq<String> fileNames = getParquetFileNames(fileName);
        if (fileNames.isEmpty()) {
            return 0;
        }
        DataFrame df = sqlContext.parquetFile(fileNames);
        return df.count();
    }

    private static Seq<String> getParquetFileNames(String fileName) {
        List<String> results = Lists.newArrayList();

        // find all parquet files in the same directory
        String parentDirPath = fileName.substring(0, fileName.lastIndexOf("/"));
        File parentDir = new File(parentDirPath);
        for (File file : parentDir.listFiles()) {
            if (file.isDirectory() && file.getAbsolutePath().startsWith(fileName)) {
                results.add(file.getAbsolutePath());
            }
        }

        return scala.collection.JavaConversions.asScalaBuffer(results).toList();
    }
}
