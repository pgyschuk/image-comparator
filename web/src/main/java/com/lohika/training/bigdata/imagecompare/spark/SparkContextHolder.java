package com.lohika.training.bigdata.imagecompare.spark;

import java.text.DecimalFormat;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PreDestroy;

import org.apache.commons.lang3.concurrent.ConcurrentException;
import org.apache.commons.lang3.concurrent.LazyInitializer;
import org.apache.spark.JavaSparkListener;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.scheduler.SparkListenerApplicationEnd;
import org.apache.spark.scheduler.SparkListenerApplicationStart;
import org.apache.spark.scheduler.SparkListenerBlockManagerAdded;
import org.apache.spark.scheduler.SparkListenerBlockManagerRemoved;
import org.apache.spark.scheduler.SparkListenerBlockUpdated;
import org.apache.spark.scheduler.SparkListenerEnvironmentUpdate;
import org.apache.spark.scheduler.SparkListenerExecutorAdded;
import org.apache.spark.scheduler.SparkListenerExecutorMetricsUpdate;
import org.apache.spark.scheduler.SparkListenerExecutorRemoved;
import org.apache.spark.scheduler.SparkListenerJobEnd;
import org.apache.spark.scheduler.SparkListenerJobStart;
import org.apache.spark.scheduler.SparkListenerStageCompleted;
import org.apache.spark.scheduler.SparkListenerStageSubmitted;
import org.apache.spark.scheduler.SparkListenerTaskEnd;
import org.apache.spark.scheduler.SparkListenerTaskGettingResult;
import org.apache.spark.scheduler.SparkListenerTaskStart;
import org.apache.spark.scheduler.SparkListenerUnpersistRDD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SparkContextHolder extends LazyInitializer<JavaSparkContext>{

    SparkListener sparkListener;
    @Autowired
    SparkConf sparkConf;
    @Value("${spark.useembedded}")
    private Boolean useEmbeddedSpark;

    @Override
    protected JavaSparkContext initialize() throws ConcurrentException {
        if (useEmbeddedSpark.booleanValue()) {
            sparkConf = new SparkConf().setAppName("Main1");
            sparkConf.setMaster("local[1]");
        }
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        sparkListener = new SparkListener();
        sc.sc().addSparkListener(sparkListener);
        return sc;
    }

    @PreDestroy
    void shutdown() throws ConcurrentException {
        get().stop();
    }

    public SparkListener getSparkListener() throws ConcurrentException {
        get();
        return sparkListener;
    }

    public static class SparkListener extends JavaSparkListener {
        AtomicInteger progressStartValue = new AtomicInteger();
        AtomicInteger progressCounter = new AtomicInteger();

        public void resetProgress(int startValue) {
            progressStartValue.set(startValue);
            progressCounter.set(startValue);
        }

        public int getStartValue() {
            return progressStartValue.get();
        }

        public int getProgressCounter() {
            return progressCounter.get();
        }

        public String getProgress() {
            float progress = getStartValue() > 0 ? ((getStartValue() - getProgressCounter()) * 100 / getStartValue()) : 0;
            return new DecimalFormat("#.##").format(progress);
        }

        @Override
        public void onUnpersistRDD(SparkListenerUnpersistRDD arg0) {
            System.err.println("onUnpersistRDD");
        }

        @Override
        public void onTaskStart(SparkListenerTaskStart arg0) {
            System.err.println("onTaskStart " + progressCounter.get());
            if (progressCounter.decrementAndGet() < 0) {
                progressCounter.set(0);
            }
        }

        @Override
        public void onTaskGettingResult(SparkListenerTaskGettingResult arg0) {
            System.err.println("onTaskGettingResult");
        }

        @Override
        public void onTaskEnd(SparkListenerTaskEnd arg0) {
            System.err.println("onTaskEnd " + progressCounter.get());
            if (progressCounter.decrementAndGet() < 0) {
                progressCounter.set(0);
            }
        }

        @Override
        public void onStageSubmitted(SparkListenerStageSubmitted arg0) {
            System.err.println("onStageSubmitted");
        }

        @Override
        public void onStageCompleted(SparkListenerStageCompleted arg0) {
            System.err.println("onStageCompleted");
        }

        @Override
        public void onJobStart(SparkListenerJobStart arg0) {
            System.err.println("onJobStart");
        }

        @Override
        public void onJobEnd(SparkListenerJobEnd arg0) {
            System.err.println("onJobEnd");
        }

        @Override
        public void onExecutorRemoved(SparkListenerExecutorRemoved arg0) {
            System.err.println("onExecutorRemoved");
        }

        @Override
        public void onExecutorMetricsUpdate(SparkListenerExecutorMetricsUpdate arg0) {
            //System.err.println("onExecutorMetricsUpdate");
        }

        @Override
        public void onExecutorAdded(SparkListenerExecutorAdded arg0) {
            System.err.println("onExecutorAdded");
        }

        @Override
        public void onEnvironmentUpdate(SparkListenerEnvironmentUpdate arg0) {
            System.err.println("onEnvironmentUpdate");
        }

        @Override
        public void onBlockUpdated(SparkListenerBlockUpdated arg0) {
            System.err.println("onBlockUpdated");
        }

        @Override
        public void onBlockManagerRemoved(SparkListenerBlockManagerRemoved arg0) {
            System.err.println("onBlockManagerRemoved");
        }

        @Override
        public void onBlockManagerAdded(SparkListenerBlockManagerAdded arg0) {
            System.err.println("onBlockManagerAdded");
        }

        @Override
        public void onApplicationStart(SparkListenerApplicationStart arg0) {
            System.err.println("onApplicationStart");
        }

        @Override
        public void onApplicationEnd(SparkListenerApplicationEnd arg0) {
            System.err.println("onApplicationEnd");
        }
    }
}
