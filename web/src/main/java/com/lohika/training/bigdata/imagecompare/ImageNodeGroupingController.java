package com.lohika.training.bigdata.imagecompare;

import java.text.DecimalFormat;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;

import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.neo4j.core.GraphDatabase;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import scala.collection.convert.Wrappers.MapWrapper;

import com.clearspring.analytics.util.Lists;
import com.google.common.collect.Maps;
import com.lohika.training.bigdata.imagecompare.model.ComparisonResult;
import com.lohika.training.bigdata.imagecompare.model.ImageNode;
import com.lohika.training.bigdata.imagecompare.repositories.neo4j.ComparisonResultRepository;
import com.lohika.training.bigdata.imagecompare.repositories.neo4j.ImageNodeRepository;

@Controller
public class ImageNodeGroupingController {

    final Logger logger = LoggerFactory.getLogger(ImageNodeGroupingController.class);

    @Autowired
    ImageNodeRepository imageNodeRepository;
    @Autowired
    ComparisonResultRepository comparisonResultRepository;
    @Autowired
    private GraphDatabase graphDatabase;
    @Autowired
    Neo4jTemplate neo4jTemplate;

    SimpleAsyncTaskExecutor executor;
    NodeGroupingTask currentNodeGroupingTask = new NodeGroupingTask();

    @PostConstruct
    void init() {
        executor = new SimpleAsyncTaskExecutor();
        executor.setConcurrencyLimit(1);
    }

    @RequestMapping("/nodegrouping")
    @ResponseBody
    public Map<String,Object> batchComparisonStop() {
        Map<String,Object> model = Maps.newHashMap();

        long count = imageNodeRepository.count();
        currentNodeGroupingTask = new NodeGroupingTask(count);
        executor.execute(currentNodeGroupingTask);

        return model;
    }

    @RequestMapping("/nodegrouping-progress")
    @ResponseBody
    public Map<String,Object> batchComparisonProgress() {
        Map<String,Object> model = Maps.newHashMap();

        if (currentNodeGroupingTask.isDone()) {
            model.put("success", true);
            model.put("numberOfGroups", currentNodeGroupingTask.getNumberOfGroups());
            model.put("numberOfMainNodes", currentNodeGroupingTask.getNumberOfMainNodes());
        } else {
            float progress = (currentNodeGroupingTask.getProgressCount() * 100) /
                    (currentNodeGroupingTask.getTotalImagesCount() > 0 ? currentNodeGroupingTask.getTotalImagesCount() : 1);
            model.put("progress", new DecimalFormat("#.##").format(progress));
        }

        return model;
    }

    private class NodeGroupingTask implements Runnable {

        long totalImagesCount = 0;
        AtomicInteger progressCounter = new AtomicInteger();
        AtomicBoolean done = new AtomicBoolean(false);
        AtomicLong numberOfGroups = new AtomicLong();
        AtomicLong numberOfMainNodes = new AtomicLong();

        public NodeGroupingTask() {
        }

        public NodeGroupingTask(long totalImagesCount) {
            this.totalImagesCount = totalImagesCount;
        }

        @Override
        public void run() {

            Iterable<ImageNode> allNodes = imageNodeRepository.findAll();
            Iterator<ImageNode> it = allNodes.iterator();

            int updateBatchSize = 100;

            // reset group id for all nodes
            while (it.hasNext()) {
                int index = 0;
                try (Transaction tx = graphDatabase.beginTx()) {
                    while (it.hasNext()) {
                        if (index == updateBatchSize) {
                            break;
                        }
                        ImageNode imageNode = it.next();
                        imageNode.setGroupId(-1L);
                        imageNode.setMainNode(false);
                        imageNodeRepository.save(imageNode);
                        index++;
                    }
                    tx.success();
                }
            }

            allNodes = imageNodeRepository.findAll();
            it = allNodes.iterator();

            long currentGroupId = 0;
            while (it.hasNext()) {
                int index = 0;
                try (Transaction tx = graphDatabase.beginTx()) {
                    while (it.hasNext()) {
                        if (index == updateBatchSize) {
                            break;
                        }
                        ImageNode imageNode = it.next();

                        // if this node has no group id, try to get the id from connected ImageNode
                        long groupId = findGroupIdByConnections(imageNode);

                        if (groupId == -1) {
                            // go farther down the connections to find group id
                            for (ComparisonResult comparisonResult : imageNode.getComparisonResults()) {
                                ImageNode otherImg = neo4jTemplate.fetch(comparisonResult.getOtherImg());
                                if (otherImg.getPath().equals(imageNode.getPath())) {
                                    otherImg = neo4jTemplate.fetch(comparisonResult.getThisImg());
                                }
                                groupId = findGroupIdByConnections(otherImg);
                                if (groupId != -1) {
                                    break;
                                }
                            }
                        }

                        // if group id still not found, assign next group id
                        if (groupId == -1) {
                            groupId = currentGroupId;
                            currentGroupId++;
                        }

                        imageNode.setGroupId(groupId);
                        imageNodeRepository.save(imageNode);

                        progressCounter.incrementAndGet();
                        index++;
                    }
                    tx.success();
                }
            }

            numberOfGroups.set(currentGroupId);

            List<Object> nodes = imageNodeRepository.getGroupsWithImageCountMoreThan(3);
            List<Long> groupIds = Lists.newArrayList();
            for (Object node : nodes) {
                MapWrapper map = (MapWrapper) node;
                Long groupId = (Long) map.values().iterator().next();
                groupIds.add(groupId);
            }

            numberOfMainNodes.set(groupIds.size());

            // assign main nodes
            Iterator<Long> groupIterator = groupIds.iterator();

            while (groupIterator.hasNext()) {
                int index = 0;
                try (Transaction tx = graphDatabase.beginTx()) {
                    while (groupIterator.hasNext()) {
                        if (index == updateBatchSize) {
                            break;
                        }
                        Long groupId = groupIterator.next();

                        /*Iterable<ImageNode> imageNodes = imageNodeRepository.getByGroupId(groupId);
                        Map<String, Integer> connectionsMap = Maps.newHashMap();
                        java.util.List<java.util.Map.Entry<String,String>> pairList = Lists.newArrayList();

                        for (ImageNode node : imageNodes) {
                            for (ComparisonResult comparisonResult : node.getComparisonResults()) {

                                ImageNode otherImg = neo4jTemplate.fetch(comparisonResult.getOtherImg());
                                if (otherImg.getPath().equals(node.getPath())) {
                                    otherImg = neo4jTemplate.fetch(comparisonResult.getThisImg());
                                }

                                String[] pathArr = new String[] { node.getPath(), otherImg.getPath() };
                                Arrays.sort(pathArr);

                                Map.Entry<String,String> pair = new AbstractMap.SimpleEntry<>(pathArr[0], pathArr[1]);

                                if (!pairList.contains(pair)) {
                                    pairList.add(pair);

                                    // increment this image connections count
                                    if (connectionsMap.get(node.getPath()) == null) {
                                        connectionsMap.put(node.getPath(), 0);
                                    }
                                    connectionsMap.put(node.getPath(), connectionsMap.get(node.getPath()) + 1);

                                    // increment other image connections count
                                    if (connectionsMap.get(otherImg.getPath()) == null) {
                                        connectionsMap.put(otherImg.getPath(), 0);
                                    }
                                    connectionsMap.put(otherImg.getPath(), connectionsMap.get(otherImg.getPath()) + 1);
                                }
                            }
                        }

                        Map.Entry<String, Integer> maxEntry = null;

                        for (Map.Entry<String, Integer> entry : connectionsMap.entrySet()) {
                            if (maxEntry == null
                                    || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                                maxEntry = entry;
                            }
                        }

                        ImageNode mainNode = imageNodeRepository.findByPath(maxEntry.getKey());*/

                        ImageNode mainNode = imageNodeRepository.getMostPopularInGroup(groupId);
                        mainNode.setMainNode(true);
                        imageNodeRepository.save(mainNode);

                        index++;
                    }
                    tx.success();
                }
            }

            done.set(true);
        }

        private long findGroupIdByConnections(ImageNode imageNode) {
            long groupId = -1;
            for (ComparisonResult comparisonResult : imageNode.getComparisonResults()) {
                ImageNode otherImg = neo4jTemplate.fetch(comparisonResult.getOtherImg());
                if (otherImg.getPath().equals(imageNode.getPath())) {
                    otherImg = neo4jTemplate.fetch(comparisonResult.getThisImg());
                }
                if (otherImg.getGroupId() != -1) {
                    groupId = otherImg.getGroupId();
                    break;
                }
            }
            return groupId;
        }

        public int getProgressCount() {
            return progressCounter.get();
        }

        public long getTotalImagesCount() {
            return totalImagesCount;
        }

        public boolean isDone() {
            return done.get();
        }

        public long getNumberOfGroups() {
            return numberOfGroups.get();
        }

        public long getNumberOfMainNodes() {
            return numberOfMainNodes.get();
        }
    }
}