package com.lohika.training.bigdata.imagecompare;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.apache.spark.sql.SaveMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lohika.training.bigdata.imagecompare.library.type.Image;
import com.lohika.training.bigdata.imagecompare.spark.SparkContextHolder;
import com.lohika.training.bigdata.imagecompare.util.FileUtils;
import com.lohika.training.bigdata.imagecompare.util.ImageConverter;
import com.lohika.training.bigdata.imagecompare.util.ParquetUtils;

@Controller
public class BatchUploadController {

    final Logger logger = LoggerFactory.getLogger(BatchUploadController.class);

    @Value("${path.images_root}")
    private String imagePath;
    private String rootImagePath;
    @Value("${path.interest_points_file}")
    private String interestPointsFile;
    private String interestPointsFilePath;

    @Value("${images.processing.batchsize}")
    private Integer imagesBatchSize;
    @Value("${images.processing.use_fixed_partitions}")
    private Boolean useFixedPartitions;

    @Autowired
    ImageBrowser imageBrowser;

    @Autowired
    SparkContextHolder sparkContextHolder;

    SimpleAsyncTaskExecutor executor;
    BatchUploadTask currentUploadTask = new BatchUploadTask();

    @PostConstruct
    void init() {
        this.rootImagePath = System.getProperty("user.home") + imagePath;
        this.interestPointsFilePath = System.getProperty("user.home") + interestPointsFile;
        executor = new SimpleAsyncTaskExecutor();
        executor.setConcurrencyLimit(1);
    }

    @RequestMapping(path = "/batchupload", method = RequestMethod.GET)
    String showBatchUpload(Model model) {
        model.addAttribute("rootPath", rootImagePath);
        return "batchupload";
    }

    @RequestMapping(path = "/batchupload", method = RequestMethod.POST)
    String batchUpload(Model model) throws Exception {

        Path path = FileSystems.getDefault().getPath(rootImagePath);
        long totalImagesCount = ParquetUtils.getImageCount(interestPointsFilePath, sparkContextHolder.get());

        currentUploadTask = new BatchUploadTask(path, totalImagesCount, interestPointsFilePath, sparkContextHolder);
        executor.execute(currentUploadTask);

        return "batchupload";
    }

    @RequestMapping("/batchupload-progress")
    @ResponseBody
    public Map<String,Object> batchUploadProgress() {
        Map<String,Object> model = Maps.newHashMap();

        if (currentUploadTask.isDone()) {
            model.put("totalAdded", currentUploadTask.getNewImagesCount());
            model.put("totalImages", currentUploadTask.getTotalImagesCount());
        } else {
            float progress = currentUploadTask.getNewImagesCount() > 0 ?
                    (currentUploadTask.getProgressCount() * 100) / currentUploadTask.getNewImagesCount()
                    : 0f;
            model.put("progress", new DecimalFormat("#.##").format(progress));
        }

        return model;
    }

    private class BatchUploadTask implements Runnable {

        Path path;
        long totalImagesCount = 0;
        int newImagesCount = 0;
        String fileName;
        SparkContextHolder sparkContextHolder;
        AtomicBoolean done = new AtomicBoolean(false);

        AtomicInteger progressCounter = new AtomicInteger();
        ImageConverter imageConverter;

        private BatchUploadTask() {}

        public BatchUploadTask(Path path, long totalImagesCount, String fileName, SparkContextHolder sparkContextHolder) {
            this.path = path;
            this.totalImagesCount = totalImagesCount;
            this.fileName = fileName;
            this.sparkContextHolder = sparkContextHolder;
            this.imageConverter = new ImageConverter(rootImagePath);
        }

        @Override
        public void run() {
            try {
                final List<String> imagePaths = Lists.newArrayList();
                final List<String> newImagePaths = Lists.newArrayList();

                logger.info("Searching for new images...");

                Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        String path = FileUtils.getRelativePath(file.toString(), rootImagePath);
                        imagePaths.add(path);

                        if (imagePaths.size() >= imagesBatchSize) {
                            try {
                                List<String> newPaths = ParquetUtils.findNewImagePaths(fileName, sparkContextHolder.get(), imagePaths);
                                newImagePaths.addAll(newPaths);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            imagePaths.clear();
                        }

                        return FileVisitResult.CONTINUE;
                    }
                });

                if (imagePaths.size() > 0) {
                    newImagePaths.addAll(ParquetUtils.findNewImagePaths(fileName, sparkContextHolder.get(), imagePaths));
                }

                logger.info("Found " + newImagePaths.size() + " new images");

                if (newImagePaths.size() > 0) {
                    List<List<String>> partitions = Lists.partition(newImagePaths, imagesBatchSize);

                    newImagesCount = newImagePaths.size();
                    if (useFixedPartitions) {
                        newImagesCount -= partitions.get(partitions.size() - 1).size();
                        logger.info("WARN: " + partitions.get(partitions.size() - 1).size()
                                + " new images will be skiped, because we are using fixed partitions of size " + imagesBatchSize);
                    }

                    for (int i = 0; i < partitions.size(); i++) {
                        List<String> paths = partitions.get(i);
                        if (i < (partitions.size() - 1) || !useFixedPartitions) {
                            writeImages(paths);
                            progressCounter.addAndGet(paths.size());
                        }
                    }
                }

                done.set(true);
                logger.info("Done!");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void writeImages(List<String> imagePaths) throws IOException {
            try {
                logger.info("Calculating interest points...");
                List<Image> images = Lists.newArrayList();

                for (String path : imagePaths) {
                    File imageFile = new File(FileUtils.getAbsolutePath(path, rootImagePath));
                    if (imageFile.exists()) {
                        images.add(imageConverter.toImage(imageFile));
                    }
                }

                logger.info("Writing images...");

                ParquetUtils.write(images, fileName, sparkContextHolder.get(), SaveMode.Append);


                logger.info("Images written " + imagePaths.size());
            } catch (Exception e) {
                throw new IOException(e);
            }
        }

        public int getProgressCount() {
            return progressCounter.get();
        }

        public long getNewImagesCount() {
            return newImagesCount;
        }

        public long getTotalImagesCount() {
            return totalImagesCount + newImagesCount;
        }

        public boolean isDone() {
            return done.get();
        }
    }
}
