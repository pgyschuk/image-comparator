package com.lohika.training.bigdata.imagecompare;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.concurrent.ConcurrentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Maps;
import com.lohika.training.bigdata.imagecompare.model.DirectoryItem;
import com.lohika.training.bigdata.imagecompare.repositories.jpa.DirectoryItemRepository;
import com.lohika.training.bigdata.imagecompare.repositories.neo4j.ImageNodeRepository;
import com.lohika.training.bigdata.imagecompare.util.FileUtils;

@RestController
public class ImageBrowserController {

    @Value("${path.images_root}")
    private String imagePath;
    private String rootImagePath;
    @Autowired
    private ImageNodeRepository imageNodeRepository;


    @PostConstruct
    void init() {
        this.rootImagePath = System.getProperty("user.home") + imagePath;
    }

    @Autowired
    ImageBrowser imageBrowser;
    @Autowired
    DirectoryItemRepository directoryItemRepository;;

    @RequestMapping("/browse")
    public Map<String,Object> browse(@RequestParam(required=false) Long itemId) throws ConcurrentException, Exception {
        Map<String,Object> model = Maps.newHashMap();
        String path = rootImagePath;
        if (itemId != null) {
            DirectoryItem currentDirItem = directoryItemRepository.findOne(itemId);
            path = FileUtils.getAbsolutePath(currentDirItem.getPath(), rootImagePath);
            model.put("totalCount", imageNodeRepository.getCountByPath(currentDirItem.getPath()));
        } else {
            model.put("totalCount", imageNodeRepository.count());
        }
        model.put("images", imageBrowser.browseDir(path));
        model.put("path", path);
        return model;
    }
}
