package com.lohika.training.bigdata.surf;

import java.io.Serializable;

/**
 * Created by pgyschuk on 12/13/15.
 */
public class Pair<K, V> implements Serializable {
    K key;
    V value;

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }
}
