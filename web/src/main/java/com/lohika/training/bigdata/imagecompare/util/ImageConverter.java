package com.lohika.training.bigdata.imagecompare.util;

import ij.ImagePlus;
import ij.io.Opener;

import java.io.File;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;
import com.lohika.training.bigdata.imagecompare.library.type.Image;
import com.lohika.training.bigdata.imagecompare.library.type.StoredInterestPoint;
import com.lohika.training.bigdata.surf.IJFacade;
import com.lohika.training.bigdata.surf.IntegralImage;
import com.lohika.training.bigdata.surf.InterestPoint;
import com.lohika.training.bigdata.surf.Params;

public class ImageConverter {

    final String rootPath;

    public ImageConverter(String rootPath) {
        this.rootPath = rootPath;
    }

    public Image toImage(File file) {
        Image image = new Image();
        image.setPath(FileUtils.getRelativePath(file, rootPath));
        Image.setStoredPois(image, getStoredPOIs(file));
        return image;
    }

    private List<StoredInterestPoint> getStoredPOIs(File file) {
        ImagePlus image = new Opener().openImage(file.getAbsolutePath());
        if (image == null) {
            // in case we couldn't read image, or image is invalid
            return Collections.emptyList();
        }
        IntegralImage intImg = new IntegralImage(image.getProcessor(), true, false);
        List<InterestPoint> pois = IJFacade.detectAndDescribeInterestPoints(intImg, new Params());

        List<StoredInterestPoint> result = Lists.newArrayList();
        for (InterestPoint p : pois) {
            result.add(toStoredInterestPoint(p));
        }
        return result;
    }

    private StoredInterestPoint toStoredInterestPoint(InterestPoint p) {
        StoredInterestPoint result = new StoredInterestPoint();
        result.setSign(p.sign);
        result.setDescriptor(BitUtils.pack3IntArray512(BitUtils.floatToIntArray(p.getDescriptor())));
        return result;
    }
}
