package com.lohika.training.bigdata.imagecompare.spark;

import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:spark.properties")
public class SparkContextConfiguration {

    @Value("${spark.master}")
    private String master;

    @Value("${spark.application-name}")
    private String applicationName;

    @Value("${spark.distributed-libraries}")
    private String[] distributedLibraries;

    @Value("${spark.cores.max}")
    private String coresMax;

    @Value("${spark.executor.memory}")
    private String executorMemory;

    @Value("${spark.serializer}")
    private String serializer;

    @Value("${spark.kryoserializer.buffer.max}")
    private String sparkKryoserializerBufferMax;

    @Value("${spark.sql.shuffle.partitions}")
    private String sqlShufflePartitions;

    @Value("${spark.default.parallelism}")
    private String defaultParallelism;

    @Bean
    public SparkConf sparkConfiguration() {
        return sparkConfigurationBuilder().buildSparkConfiguration();
    }

    @Bean
    public SparkConfigurationBuilder sparkConfigurationBuilder() {
        return new SparkConfigurationBuilder(master, applicationName, distributedLibraries, sparkProperties());
    }

    private Map<String, String> sparkProperties() {
        Map<String, String> sparkProperties = new HashMap<>();
        sparkProperties.put("spark.cores.max", coresMax);
        sparkProperties.put("spark.executor.memory", executorMemory);
        sparkProperties.put("spark.serializer", serializer);
        sparkProperties.put("spark.kryoserializer.buffer.max", sparkKryoserializerBufferMax);
        sparkProperties.put("spark.sql.shuffle.partitions", sqlShufflePartitions);
        sparkProperties.put("spark.default.parallelism", defaultParallelism);

        return sparkProperties;
    }
}
