package com.lohika.training.bigdata.imagecompare;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.concurrent.ConcurrentException;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Row;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.neo4j.core.GraphDatabase;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import scala.Tuple2;

import com.clearspring.analytics.util.Lists;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.lohika.training.bigdata.imagecompare.library.function.filter.FilterImagesByMatchCount;
import com.lohika.training.bigdata.imagecompare.library.function.map.ImageToPathAndTimestampFunction;
import com.lohika.training.bigdata.imagecompare.library.function.map.ImagesToMatchCountFunction;
import com.lohika.training.bigdata.imagecompare.library.type.Image;
import com.lohika.training.bigdata.imagecompare.model.ComparisonResult;
import com.lohika.training.bigdata.imagecompare.model.ImageNode;
import com.lohika.training.bigdata.imagecompare.repositories.neo4j.ComparisonResultRepository;
import com.lohika.training.bigdata.imagecompare.repositories.neo4j.ImageNodeRepository;
import com.lohika.training.bigdata.imagecompare.spark.SparkContextHolder;
import com.lohika.training.bigdata.imagecompare.util.FileUtils;
import com.lohika.training.bigdata.imagecompare.util.ImageConverter;
import com.lohika.training.bigdata.imagecompare.util.ParquetUtils;

@Controller
public class BatchComparisonController {

    final Logger logger = LoggerFactory.getLogger(BatchComparisonController.class);

    @Value("${path.images_root}")
    private String imagePath;
    private String rootImagePath;
    @Value("${path.interest_points_file}")
    private String interestPointsFile;
    private String interestPointsFilePath;
    @Value("${path.errorslog}")
    private String errorsLogFile;
    private String errorsLogFilePath;
    @Value("${images.comparison.min_ip_match_count}")
    Integer minIPMatchCount;

    @Autowired
    SparkContextHolder sparkContextHolder;
    @Autowired
    ImageNodeRepository imageNodeRepository;
    @Autowired
    ComparisonResultRepository comparisonResultRepository;
    @Autowired
    private GraphDatabase graphDatabase;

    SimpleAsyncTaskExecutor executor;
    BatchComparisonTask currentComparisonTask = new BatchComparisonTask();

    @PostConstruct
    void init() {
        this.interestPointsFilePath = System.getProperty("user.home") + interestPointsFile;
        this.rootImagePath = System.getProperty("user.home") + imagePath;
        this.errorsLogFilePath = System.getProperty("user.home") + errorsLogFile;
        executor = new SimpleAsyncTaskExecutor();
        executor.setConcurrencyLimit(1);
    }

    @RequestMapping(path = "/batchcomparison", method = RequestMethod.GET)
    String showBatchComparison(Model model) throws ConcurrentException, Exception {
        long totalImagesCount = ParquetUtils.getImageCount(interestPointsFilePath, sparkContextHolder.get());
        model.addAttribute("availableForComparisonCount", (totalImagesCount - imageNodeRepository.count()));
        return "batchcomparison";
    }

    @RequestMapping(path = "/batchcomparison", method = RequestMethod.POST)
    String batchComparison(Model model) throws Exception {

        long totalImagesCount = ParquetUtils.getImageCount(interestPointsFilePath, sparkContextHolder.get());
        ImageConverter imageConverter = new ImageConverter(rootImagePath);

        currentComparisonTask = new BatchComparisonTask(totalImagesCount, imageConverter);
        executor.execute(currentComparisonTask);

        return "batchcomparison";
    }

    @RequestMapping("/batchcomparison-progress")
    @ResponseBody
    public Map<String,Object> batchComparisonProgress() {
        Map<String,Object> model = Maps.newHashMap();

        if (currentComparisonTask.isDone()) {
            model.put("totalProcessed", currentComparisonTask.getTotalProcessed() - currentComparisonTask.getErrorsCount());
            model.put("totalImages", currentComparisonTask.getTotalImagesCount());
            model.put("errorsCount", currentComparisonTask.getErrorsCount());
        } else {
            float progress = (currentComparisonTask.getProgressCount() * 100) /
                    (currentComparisonTask.getTotalProcessed() > 0 ? currentComparisonTask.getTotalProcessed() : 1);
            model.put("progress", new DecimalFormat("#.##").format(progress));
        }

        return model;
    }

    @RequestMapping("/batchcomparison-stop")
    @ResponseBody
    public Map<String,Object> batchComparisonStop() {
        Map<String,Object> model = Maps.newHashMap();
        currentComparisonTask.stop();
        return model;
    }

    private class BatchComparisonTask implements Runnable {

        long totalImagesCount = 0;
        long totalProcessed = 1;
        List<String> errors = Lists.newArrayList();
        ImageConverter imageConverter;
        AtomicBoolean done = new AtomicBoolean(false);
        AtomicBoolean stopped = new AtomicBoolean(false);
        AtomicInteger progressCounter = new AtomicInteger();

        private BatchComparisonTask() {}

        public BatchComparisonTask(long totalImagesCount, ImageConverter imageConverter) {
            this.totalImagesCount = totalImagesCount;
            this.imageConverter = imageConverter;
        }

        @Override
        public void run() {

            // TODO remove
            //imageNodeRepository.deleteAll();
            //comparisonResultRepository.deleteAll();

            // 1. Find the timestamp of the latest saved node in neo4j.
            ImageNode latestNode = imageNodeRepository.getLatestNode();
            long startDate = 0;
            if (latestNode != null) {
                startDate = latestNode.getTimestamp();
            }

            // 2. Get all new image paths ordered by timestamp ascending
            JavaRDD<Row> newImagePaths = null;
            try {
                newImagePaths = ParquetUtils.readPaths(interestPointsFilePath, sparkContextHolder.get(), startDate);
            } catch (ConcurrentException e1) {
                // should never happen
                e1.printStackTrace();
            }

            totalProcessed = newImagePaths.count();
            Iterator<Tuple2<String, Long>> newPathsIterator = newImagePaths
                    .map(new ImageToPathAndTimestampFunction())
                    .toLocalIterator();

            while (newPathsIterator.hasNext()) {
                Tuple2<String, Long> pathRes = newPathsIterator.next();
                String path = pathRes._1;
                logger.info("Processing '" + path + "'");

                try {
                    // 3. Construct RDD that consists of all processed images (that are in neo4j db)
                    JavaRDD<Image> allImagesRDD = ParquetUtils.read(interestPointsFilePath, sparkContextHolder.get(), startDate);

                    // 4. Create Pair RDD with current image
                    File imageFile = new File(FileUtils.getAbsolutePath(path, rootImagePath));
                    Image targetImage = imageConverter.toImage(imageFile);
                    JavaRDD<Image> currentImageRDD = sparkContextHolder.get().parallelize(ImmutableList.of(targetImage));
                    JavaPairRDD<Image, Image> pairRDD = allImagesRDD.cartesian(currentImageRDD);

                    // 5. Do the comparison
                    List<Tuple2<Image,Integer>> results = pairRDD
                            .mapToPair(new ImagesToMatchCountFunction())
                            //.map(new ImageToMatchPercentFunction())
                            .filter(new FilterImagesByMatchCount(minIPMatchCount))
                            //.sortBy(new SortByPercent(), false, 1)
                            .collect();

                    try (Transaction tx = graphDatabase.beginTx()) {

                        // 6. create new node for this image
                        ImageNode imageNode = new ImageNode();
                        imageNode.setPath(path);
                        imageNode.setTimestamp(pathRes._2);
                        imageNodeRepository.save(imageNode);

                        // 7. store comparison results, create graph connections
                        for (Tuple2<Image, Integer> tuple2 : results) {

                            ImageNode otherNode = imageNodeRepository.findByPath(tuple2._1.getPath());
                            if (otherNode == null) {
                                logger.info("Skipping '" + tuple2._1.getPath() + "', it will be processed in next iteration");
                            } else {

                                ComparisonResult res = new ComparisonResult();
                                res.setMatchCount(tuple2._2);
                                int size = tuple2._1.getPlusSignInterestPoints().length + tuple2._1.getMinusSignInterestPoints().length;
                                int percent = (int) Math.ceil(((float)tuple2._2 / size) * 100);
                                res.setMatchPercent(percent);

                                res.setThisImg(imageNode);
                                res.setOtherImg(otherNode);

                                // Make sure that connections exist
                                imageNode.addComparisonResult(res);

                                imageNodeRepository.save(imageNode);

                            }
                        }
                        tx.success();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    errors.add(path);
                }

                // 8. Reassign start date to move to next image
                startDate = pathRes._2;

                progressCounter.incrementAndGet();

                logger.info("Progress " + progressCounter.intValue() + " of " + totalProcessed);

                if (stopped.get()) {
                    totalProcessed = progressCounter.get();
                    break;
                }
            }

            // write paths of failed images to log file
            if (errors.size() > 0) {
                File errorsFile = new File(errorsLogFilePath);
                if (errorsFile.exists()) {
                    errorsFile.delete();
                }
                try {
                    errorsFile.createNewFile();

                    Path out = Paths.get(errorsLogFilePath);
                    Files.write(out, errors, Charset.defaultCharset());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            done.set(true);
        }

        public int getProgressCount() {
            return progressCounter.get();
        }

        public long getTotalImagesCount() {
            return totalImagesCount;
        }

        public long getTotalProcessed() {
            return totalProcessed;
        }

        public long getErrorsCount() {
            return errors.size();
        }

        public boolean isDone() {
            return done.get();
        }

        public void stop() {
            stopped.set(true);
        }
    }
}
