package com.lohika.training.bigdata.imagecompare.repositories;

import javax.persistence.EntityManagerFactory;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.lohika.training.bigdata.imagecompare.model"})
@EnableJpaRepositories(basePackages = {"com.lohika.training.bigdata.imagecompare.repositories.jpa"}, transactionManagerRef = "jpaTransactionManager")
@EnableNeo4jRepositories(basePackages = {"com.lohika.training.bigdata.imagecompare.repositories.neo4j"})
@EnableTransactionManagement
public class RepositoryConfiguration extends Neo4jConfiguration{

    public RepositoryConfiguration() {
        setBasePackage("com.lohika.training.bigdata.imagecompare.model");
    }

    @Bean
    GraphDatabaseService graphDatabaseService(@Value("${path.neo4jdb}") String neo4jDBFile) {
        String neo4jDBFilePath = System.getProperty("user.home") + neo4jDBFile;
        return new GraphDatabaseFactory().newEmbeddedDatabase(neo4jDBFilePath + "accessingdataneo4j.db");
    }

    @Bean(name = "jpaTransactionManager")
    @Qualifier("jpaTransactionManager")
    public JpaTransactionManager jpaTransactionManager(EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
