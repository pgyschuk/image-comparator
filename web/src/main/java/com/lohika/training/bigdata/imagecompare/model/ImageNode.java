package com.lohika.training.bigdata.imagecompare.model;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedToVia;

@NodeEntity
public class ImageNode {

    @GraphId Long id;
    String path;
    // Group of images according to comparison results. Images that have more than "images.comparison.min_ip_match_count"
    // matching points form groups in neo4j graph. This field is used to enable near real time comparison with brand new image.
    Long groupId = -1L;
    boolean mainNode = false;
    Long timestamp = System.currentTimeMillis();

    @RelatedToVia(type="COMPARISON_RESULT", direction = Direction.BOTH)
    private Set<ComparisonResult> comparisonResults = new HashSet<>();

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public Long getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
    public Set<ComparisonResult> getComparisonResults() {
        return comparisonResults;
    }
    public void setComparisonResults(Set<ComparisonResult> comparisonResults) {
        this.comparisonResults = comparisonResults;
    }
    public Long getGroupId() {
        return groupId;
    }
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }
    public boolean isMainNode() {
        return mainNode;
    }
    public void setMainNode(boolean mainNode) {
        this.mainNode = mainNode;
    }
    public void addComparisonResult(ComparisonResult res) {
        res.getThisImg().getComparisonResults().add(res);
        res.getOtherImg().getComparisonResults().add(res);
    }
}
