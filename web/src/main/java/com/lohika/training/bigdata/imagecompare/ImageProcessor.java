package com.lohika.training.bigdata.imagecompare;

import java.io.File;
import java.io.FileFilter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.lohika.training.bigdata.surf.IJFacade;
import com.lohika.training.bigdata.surf.IntegralImage;
import com.lohika.training.bigdata.surf.InterestPoint;
import com.lohika.training.bigdata.surf.Matcher;
import com.lohika.training.bigdata.surf.Pair;
import com.lohika.training.bigdata.surf.Params;
import ij.ImagePlus;
import ij.io.Opener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;


/**
 * Created by pgyschuk on 12/12/15.
 */
public class ImageProcessor implements Serializable {
    Log LOG = LogFactory.getLog(ImageProcessor.class);

    public void convertToParquet() {
        SparkConf sparkConf = new SparkConf().setAppName("FileMapper");
        sparkConf.setMaster("local[1]");
        final JavaSparkContext jsc = new JavaSparkContext(sparkConf);
        final SQLContext sqlContext = new SQLContext(jsc);


        File inputFolder = new File("/Users/pgyschuk/Projects/training/images");
        List<File> images = Arrays.asList(inputFolder.listFiles(new FileFilter() {
            @Override public boolean accept(File pathname) {
                return pathname.toString().endsWith(".jpg");
            }
        }));

        JavaRDD<File> files = jsc.parallelize(images);

        final StructType schema = getParquetSchema();

        final JavaRDD<Pair<String, List<InterestPoint>>> filePointListPairRDD = files.map(new Function<File, Pair<String, List<InterestPoint>>>() {
            @Override public Pair<String, List<InterestPoint>> call(File fileImage) throws Exception {
                List<InterestPoint> interestPoints = getInterestPoints(fileImage);
                return new Pair<String, List<InterestPoint>>(fileImage.getPath(), interestPoints);
            }
        });

        JavaRDD<List<Pair<String, InterestPoint>>> filePointPairRDD = filePointListPairRDD.map(
                new Function<Pair<String, List<InterestPoint>>, List<Pair<String, InterestPoint>>>() {
                    @Override public List<Pair<String, InterestPoint>> call(Pair<String, List<InterestPoint>> pair) throws Exception {
                        List<Pair<String, InterestPoint>> list = new ArrayList<Pair<String, InterestPoint>>();
                        for (InterestPoint interestPoint : pair.getValue()) {
                            list.add(new Pair<String, InterestPoint>(pair.getKey(), interestPoint));
                        }
                        return list;
                    }
                });

        JavaRDD<List<Row>> rowListRDD = filePointPairRDD.map(new Function<List<Pair<String, InterestPoint>>, List<Row>>() {
            List<Row> list = new ArrayList<Row>();

            @Override public List<Row> call(List<Pair<String, InterestPoint>> pairs) throws Exception {
                for (Pair<String, InterestPoint> pair : pairs) {
                    InterestPoint interestPoint = pair.getValue();
                    final String imagePath = pair.getKey();
                    Row row = RowFactory.create(imagePath, interestPoint.getX(), interestPoint.getY(), interestPoint.getDx(), interestPoint.getDy(),
                                                interestPoint.getOrientation(), interestPoint.getScale(), interestPoint.getStrength(), interestPoint.getTrace(),
                                                interestPoint.getDescriptor());
                    list.add(row);
                }
                return list;
            }
        });

        List<Row> allRows = rowListRDD.reduce(new Function2<List<Row>, List<Row>, List<Row>>() {
            @Override public List<Row> call(List<Row> rows, List<Row> rows2) throws Exception {
                List<Row> allRows = new ArrayList<Row>();
                allRows.addAll(rows);
                allRows.addAll(rows2);
                System.out.println("Processing");
                for(Row row: allRows){
                    System.out.println(row);
                }
                return allRows;
            }
        });

        JavaRDD<Row> rowRDD1 = jsc.parallelize(allRows);

        DataFrame interestPointDataFrame = sqlContext.createDataFrame(rowRDD1, schema);
        interestPointDataFrame.write().parquet("hdfs://localhost:8020/training/result/5");

    }

    private List<InterestPoint> getInterestPoints(File fileImage) {
        ImagePlus image = new Opener().openImage(fileImage.getPath());
        IntegralImage intImg1 = new IntegralImage(image.getProcessor(), true, false);
        return IJFacade.detectAndDescribeInterestPoints(intImg1, new Params());
    }


    private static StructType getParquetSchema() {
        List<StructField> fields = new LinkedList<StructField>();

        fields.add(DataTypes.createStructField("filePath", DataTypes.StringType, false));
        fields.add(DataTypes.createStructField("x", DataTypes.FloatType, true));
        fields.add(DataTypes.createStructField("y", DataTypes.FloatType, true));
        fields.add(DataTypes.createStructField("dx", DataTypes.FloatType, true));
        fields.add(DataTypes.createStructField("dy", DataTypes.FloatType, true));
        fields.add(DataTypes.createStructField("strength", DataTypes.FloatType, true));
        fields.add(DataTypes.createStructField("trace", DataTypes.FloatType, true));
        fields.add(DataTypes.createStructField("scale", DataTypes.FloatType, true));
        fields.add(DataTypes.createStructField("orientation", DataTypes.FloatType, true));
        fields.add(DataTypes.createStructField("descriptor", DataTypes.createArrayType(DataTypes.FloatType), true));

        return DataTypes.createStructType(fields);
    }

    public void search() {
        SparkConf sparkConf = new SparkConf().setAppName("FileMapper");
        sparkConf.setMaster("local[1]");
        final JavaSparkContext jsc = new JavaSparkContext(sparkConf);
        final SQLContext sqlContext = new SQLContext(jsc);
        DataFrame parquetDataFrame = sqlContext.read().parquet("hdfs://localhost:8020/training/result/5");
        JavaRDD<Row> rows = parquetDataFrame.javaRDD();
        System.out.println("Searching");
        //float x, float y, float dx, float dy, float strength, float trace, float scale, float orientation, float[] descriptor
        JavaRDD<Pair<String, InterestPoint>> filePointPairRDD = rows.map(new Function<Row, Pair<String, InterestPoint>>() {
            @Override public Pair<String, InterestPoint> call(Row row) throws Exception {
                float[] descriptor = new float[row.getList(9).size()];
               int i = 0;
                for(Float f: row.<Float>getList(9)){
                    descriptor[i] = f.floatValue();
                    i++;
                }
                InterestPoint interestPoint = new InterestPoint(row.getFloat(1), row.getFloat(2),row.getFloat(3), row.getFloat(4), row.getFloat(5),row.getFloat(6), row.getFloat(7),row.getFloat(8), descriptor );
                System.out.println(row);
                return new Pair<String, InterestPoint>(row.getString(0), interestPoint);
            }
        });

        JavaRDD<Multimap<String, InterestPoint>> filePointListMapRDD = filePointPairRDD.map(
                new Function<Pair<String, InterestPoint>, Multimap<String, InterestPoint>>() {
                    @Override public Multimap<String, InterestPoint> call(Pair<String, InterestPoint> stringInterestPointPair) throws Exception {
                        Multimap<String, InterestPoint> filePointListMap = ArrayListMultimap.create();
                        filePointListMap.put(stringInterestPointPair.getKey(), stringInterestPointPair.getValue());
                        return filePointListMap;
                    }
                });

        final Multimap<String, InterestPoint> finalMap = filePointListMapRDD.reduce(
                new Function2<Multimap<String, InterestPoint>, Multimap<String, InterestPoint>, Multimap<String, InterestPoint>>() {
                    @Override public Multimap<String, InterestPoint> call(Multimap<String, InterestPoint> stringInterestPointMultimap,
                                                                          Multimap<String, InterestPoint> stringInterestPointMultimap2) throws Exception {
                        stringInterestPointMultimap.putAll(stringInterestPointMultimap2);
                        return stringInterestPointMultimap;
                    }
                });

        JavaRDD<String> keys = jsc.parallelize(new ArrayList<String>(finalMap.keySet()));
        JavaRDD<String> resultRDD = keys.map(new Function<String, String>() {
            @Override public String call(String s) throws Exception {
                String filePath = "/Users/pgyschuk/Projects/training/images/899a7904e4ea4cbb5822a56bd509a0c3638fd6bcea7ae37a81595bcb51e71c35_400.jpg";
                List<InterestPoint> interestPoints1 = getInterestPoints(new File(filePath));
                List<InterestPoint> interestPoints2 = new ArrayList<InterestPoint>(finalMap.get(s));

                Map<InterestPoint, InterestPoint> matchedPoints = Matcher.findMathes(interestPoints1, interestPoints2);

                Map<InterestPoint, InterestPoint> matchedPointsReverse = Matcher.findMathes(interestPoints2, interestPoints1);
                //matchedPoints = intersection(matchedPoints, matchedPointsReverse);
                String result = "File 1 " + s + "\n" +
                                "File 2 " + filePath + "\n" +
                                "matchedPoints" + matchedPoints.size() + "\n" +
                                "matchedPercent" + (int) (((float) matchedPoints.size() / interestPoints1.size()) * 100);
                return result;
            }
        });

        String s = resultRDD.reduce(new Function2<String, String, String>() {
            @Override public String call(String s, String s2) throws Exception {
                return s + "\n\n" + s2;
            }
        });
        System.out.println(s);
    }

    /**
     * Return a new Map contaning only those entries from map1 that also contain (as reversed key/value paar) in map2.
     */
    Map<InterestPoint, InterestPoint> intersection(Map<InterestPoint, InterestPoint> map1, Map<InterestPoint, InterestPoint> map2) {
        // take only those points that matched in the reverse comparison too
        Map<InterestPoint, InterestPoint> result = new HashMap<InterestPoint, InterestPoint>();
        for (InterestPoint ipt1 : map1.keySet()) {
            InterestPoint ipt2 = map1.get(ipt1);
            if (ipt1 == map2.get(ipt2)) {
                result.put(ipt1, ipt2);
            }
        }
        return result;
    }


    public static void main(String[] args) throws Exception {
        ImageProcessor imageProcessor = new ImageProcessor();
        imageProcessor.convertToParquet();
        imageProcessor.search();
    }
}