package com.lohika.training.bigdata.imagecompare.util;

import java.io.File;

import org.springframework.util.StringUtils;

public class FileUtils {

    public static File[] listDir(File dir, String rootPath) {
        if (!dir.exists() || !dir.isDirectory()) {
            throw new IllegalArgumentException("Invalid directory " + dir);
        }
        return dir.listFiles();
    }

    public static String getRelativePath(File file, String rootPath) {
        return getRelativePath(file.getAbsolutePath(), rootPath);
    }

    public static String getRelativePath(String path, String rootPath) {
        String result = path.replace(rootPath, "");
        if (result.startsWith("/")) {
            result = result.substring(1);
        }
        if (result.endsWith("/")) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    public static String getAbsolutePath(String path, String rootPath) {
        if (path.startsWith("/")) {
            return path;
        } else if (StringUtils.hasLength(path)) {
            return rootPath + "/" + path;
        } else {
            return rootPath;
        }
    }
}
