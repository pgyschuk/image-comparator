package com.lohika.training.bigdata.test2;

import com.google.common.collect.Lists;
import com.lohika.training.bigdata.surf.IJFacade;
import com.lohika.training.bigdata.surf.IntegralImage;
import com.lohika.training.bigdata.surf.InterestPoint;
import com.lohika.training.bigdata.surf.Params;
import ij.ImagePlus;
import ij.io.Opener;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Computes an approximation to pi Usage: JavaSparkPi [slices]
 */
public final class Main1 {

    public static void main(String[] args) throws Exception {
        SparkConf sparkConf = new SparkConf().setAppName("Main1");
        sparkConf.setMaster("local[1]");


        JavaSparkContext jsc = new JavaSparkContext(sparkConf);

        int slices = (args.length == 1) ? Integer.parseInt(args[0]) : 2;
        int n = 100000 * slices;
        //List<Integer> l = new ArrayList<Integer>(n);
        //for (int i = 0; i < n; i++)
        //{
        //    l.add(i);
        //}


        //File inputFolder = new File("/Users/pgyschuk/Projects/training/images");
        //List<File> images =  Arrays.asList(inputFolder.listFiles());
        //
        //        JavaRDD < File > dataSet = jsc.parallelize(images, slices);
        //
        //int count = dataSet.map(new Function<File, List<InterestPoint>>() {
        //    public List<InterestPoint> call(File fileImage) {
        //        ImagePlus image = new Opener().openImage(fileImage.getPath());
        //        IntegralImage intImg1 = new IntegralImage(image.getProcessor(), true);
        //        List<InterestPoint> ipts1 = IJFacade.detectAndDescribeInterestPoints(intImg1, new Params());
        //        return ipts1;
        //    }
        //})
        //        .reduce(new Function2<Integer, Integer, Integer>() {
        //    public Integer call(Integer integer, Integer integer2) {
        //        return integer + integer2;
        //    }
        //});

        //System.out.println("Pi is roughly " + 4.0 * count / n);

        jsc.stop();
    }

    private List<InterestPoint> getPOIs(File fileName) throws IOException {
        ImagePlus image = new Opener().openImage(fileName.getPath());
        IntegralImage intImg1 = new IntegralImage(image.getProcessor(), true, false);
        List<InterestPoint> ipts1 = IJFacade.detectAndDescribeInterestPoints(intImg1, new Params());
        return ipts1;
    }
}