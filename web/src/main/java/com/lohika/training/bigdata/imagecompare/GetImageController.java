package com.lohika.training.bigdata.imagecompare;

import ij.ImagePlus;
import ij.io.Opener;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.io.ByteStreams;
import com.lohika.training.bigdata.imagecompare.model.DirectoryItem;
import com.lohika.training.bigdata.imagecompare.repositories.jpa.DirectoryItemRepository;
import com.lohika.training.bigdata.imagecompare.util.FileUtils;
import com.lohika.training.bigdata.surf.IJFacade;
import com.lohika.training.bigdata.surf.IntegralImage;
import com.lohika.training.bigdata.surf.InterestPoint;
import com.lohika.training.bigdata.surf.Matcher;
import com.lohika.training.bigdata.surf.Params;

@Controller
public class GetImageController {

    @Value("${path.images_root}")
    private String imagePath;
    private String rootImagePath;
    @Value("${imagedirectory.previews_count}")
    private int imageDirectoryPreviewsCount;

    @Autowired
    DirectoryItemRepository directoryItemRepository;

    @PostConstruct
    void init() {
        this.rootImagePath = System.getProperty("user.home") + imagePath;
    }

    @ResponseBody
    @RequestMapping(path = "/image/{imageId:.+}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public void getImage(@PathVariable Long imageId, @RequestParam(required=false) Boolean showPois,
            @RequestParam(required=false) Long compareTo, HttpServletResponse response) throws IOException {

        response.setContentType("image/jpeg");

        String imagePath = getImagePath(imageId);

        if (compareTo == null) {
            // just output the image
            if (showPois == null) {
                File imgFile = new File(imagePath);
                response.setContentLengthLong(imgFile.length());
                try (InputStream is = new FileInputStream(imgFile)) {
                    ByteStreams.copy(is, response.getOutputStream());
                }
            } else {
                ImagePlus imagePlus = new Opener().openImage(imagePath);
                List<InterestPoint> pois = getPOIs(imagePlus);
                BufferedImage img = getBufferedImageWithPois(imagePlus, pois, 1);
                ImageIO.write(img, "jpg", response.getOutputStream());
            }
        } else {
            // combine 2 images into 1 for comparison

            BufferedImage img1 = null;
            BufferedImage img2 = null;

            String imageToComparePath = getImagePath(compareTo);

            if (showPois == null) {
                // load both images
                img1 = ImageIO.read(new File(imagePath));
                img2 = ImageIO.read(new File(imageToComparePath));
            } else {
                ImagePlus image1 = new Opener().openImage(imagePath);
                ImagePlus image2 = new Opener().openImage(imageToComparePath);

                // find matching points and draw them on the images
                List<InterestPoint> pois1 = getPOIs(image1);
                List<InterestPoint> pois2 = getPOIs(image2);

                Map<InterestPoint, InterestPoint> matchingPois = Matcher.findMathes(pois1, pois2, true);

                // resize images by width
                int img1NewHeight = image1.getHeight();
                int img2NewHeight = image2.getHeight();

                if (image1.getWidth() > image2.getWidth()) {
                    img1NewHeight = (int) (image1.getHeight() * ((float)image2.getWidth() / image1.getWidth()));
                } else if (image2.getWidth() > image1.getWidth()) {
                    img2NewHeight = (int) (image2.getHeight() * ((float)image1.getWidth() / image2.getWidth()));
                }

                img1 = getBufferedImageWithPois(image1, matchingPois.keySet(), (float) image1.getHeight() / img1NewHeight);
                img2 = getBufferedImageWithPois(image2, matchingPois.values(), (float) image2.getHeight() / img2NewHeight);
            }

            BufferedImage resultingImage = joinBufferedImage(img1, img2);
            ImageIO.write(resultingImage, "jpg", response.getOutputStream());
        }
    }

    private String getImagePath(Long imageId) {
        DirectoryItem image = directoryItemRepository.findOne(imageId);
        if (image.isDirectory()) {
            throw new IllegalStateException("Directory item " + image.getPath() + " is a directory");
        }
        return FileUtils.getAbsolutePath(image.getPath(), rootImagePath);
    }

    private BufferedImage getBufferedImageWithPois(ImagePlus imagePlus, Collection<InterestPoint> pois, float pointScale) {
        ImageProcessor imageProcessor = imagePlus.getProcessor()/*.convertToRGB()*/;

        // draw pois on image copy
        Params params = new Params();
        params.setLineWidth(Math.round(params.getLineWidth() * pointScale));
        for (InterestPoint p : pois) {
            IJFacade.drawSingleInterestPoint(imageProcessor, params, p);
        }

        return imagePlus.getBufferedImage();
    }

    private List<InterestPoint> getPOIs(ImagePlus image) throws IOException {
        IntegralImage intImg = new IntegralImage(image.getProcessor(), true, false);
        return IJFacade.detectAndDescribeInterestPoints(intImg, new Params());
    }

    private BufferedImage joinBufferedImage(BufferedImage bufferedImg1, BufferedImage bufferedImg2) {

        Image img1 = bufferedImg1;
        Image img2 = bufferedImg2;

        // resize images by width
        if (img1.getWidth(null) > img2.getWidth(null)) {
            int newHeidht = (int) (img1.getHeight(null) * ((float)img2.getWidth(null) / img1.getWidth(null)));
            img1 = img1.getScaledInstance(img2.getWidth(null), newHeidht, Image.SCALE_SMOOTH);
        } else if (img2.getWidth(null) > img1.getWidth(null)) {
            int newHeidht = (int) (img2.getHeight(null) * ((float)img1.getWidth(null) / img2.getWidth(null)));
            img2 = img2.getScaledInstance(img1.getWidth(null), newHeidht, Image.SCALE_SMOOTH);
        }

        int offset = 20;
        int wid = img1.getWidth(null)+img2.getWidth(null)+offset;
        int height = Math.max(img1.getHeight(null),img2.getHeight(null))+offset;
        //create a new buffer and draw two image into the new image
        BufferedImage newImage = new BufferedImage(wid,height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = newImage.createGraphics();
        Color oldColor = g2.getColor();
        //fill background
        g2.setPaint(Color.WHITE);
        g2.fillRect(0, 0, wid, height);
        //draw image
        g2.setColor(oldColor);

        // center both images vertically using vertical offset
        int vertOffset1 = 0;
        int vertOffset2 = 0;
        if (img1.getHeight(null) > img2.getHeight(null)) {
            vertOffset2 = (img1.getHeight(null) - img2.getHeight(null)) / 2;
        } else if (img2.getHeight(null) > img1.getHeight(null)) {
            vertOffset1 = (img2.getHeight(null) - img1.getHeight(null)) / 2;
        }

        g2.drawImage(img1, 0, vertOffset1, null);
        g2.drawImage(img2, img1.getWidth(null)+offset, vertOffset2, null);
        g2.dispose();
        return newImage;
    }
}
