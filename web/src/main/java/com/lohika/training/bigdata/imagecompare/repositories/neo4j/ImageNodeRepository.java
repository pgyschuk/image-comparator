package com.lohika.training.bigdata.imagecompare.repositories.neo4j;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.lohika.training.bigdata.imagecompare.model.ImageNode;

public interface ImageNodeRepository extends GraphRepository<ImageNode> {

    ImageNode findByPath(String path);

    @Query("MATCH (n) RETURN n ORDER BY n.timestamp DESC LIMIT 1")
    ImageNode getLatestNode();

    @Query("MATCH (n) WITH n.groupId as g, count(n.groupId) as c WHERE c > {0} RETURN g ORDER BY c DESC")
    List<Object> getGroupsWithImageCountMoreThan(int minCount);

    Iterable<ImageNode> getByGroupId(Long groupId);

    @Query("MATCH (n)-[r]-() WITH n as n1, count(r) as c WHERE n1.groupId={0} RETURN n1 ORDER BY c DESC LIMIT 1")
    ImageNode getMostPopularInGroup(Long groupId);

    @Query("MATCH (n) WHERE n.mainNode = true RETURN n")
    Iterable<ImageNode> getMainNodes();

    @Query("MATCH (n) WHERE n.path =~ ({0} + '.*') RETURN count(*) as c")
    Long getCountByPath(String path);
}
