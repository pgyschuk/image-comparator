package com.lohika.training.bigdata.surf;

import ij.ImagePlus;
import ij.io.Opener;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.clearspring.analytics.util.Lists;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;

import static org.junit.Assert.assertEquals;

public class MatcherPerformanceTest {

    final Logger logger = LoggerFactory.getLogger(MatcherPerformanceTest.class);

    static final String IMG1_NAME = "20151228_113453.jpg";
    static final String IMG2_NAME = "LvivOperaHouse.JPG";

    static int TARGET_MATCHED_POINTS_COUNT = 335;

    List<InterestPoint> img1InterestPoints;
    List<InterestPoint> img2InterestPoints;

    List<InterestPointFloat> img1InterestPointsF = Lists.newArrayList();
    List<InterestPointFloat> img2InterestPointsF = Lists.newArrayList();
    List<InterestPointInt> img1InterestPointsI = Lists.newArrayList();
    List<InterestPointInt> img2InterestPointsI = Lists.newArrayList();
    List<InterestPointShort> img1InterestPointsS = Lists.newArrayList();
    List<InterestPointShort> img2InterestPointsS = Lists.newArrayList();

    @Before
    public void init() throws IOException {
        this.img1InterestPoints = getPOIs(getImgFilePath(IMG1_NAME));
        this.img2InterestPoints = getPOIs(getImgFilePath(IMG2_NAME));

        for (InterestPoint ip : img1InterestPoints) {
            img1InterestPointsF.add(new InterestPointFloat(ip.sign, ip.descriptor));
            img1InterestPointsI.add(new InterestPointInt(ip.sign, floatToIntArray(ip.descriptor)));
        }

        for (InterestPoint ip : img2InterestPoints) {
            img2InterestPointsF.add(new InterestPointFloat(ip.sign, ip.descriptor));
            img2InterestPointsI.add(new InterestPointInt(ip.sign, floatToIntArray(ip.descriptor)));
        }
    }

    int[] floatToIntArray(float[] fa) {
        int[] arr = new int[fa.length];
        for (int i = 0; i < fa.length; i++) {
            arr[i] = (int) (fa[i] * 1000);
        }
        return arr;
    }

    @Test
    public void testMatcher() {
        int pointsCount = 0;
        int numOfTries = 20;
        int warmupCount = 2;

        long start = 0;//System.currentTimeMillis();
        logger.info("Test started...");
        for (int i = 0; i < numOfTries; i++) {
            //pointsCount = testOriginalMatcher(img1InterestPoints, img2InterestPoints);
            //pointsCount = testMatcherFloat(img1InterestPointsF, img2InterestPointsF);
            //pointsCount = testMatcherInt(img1InterestPointsI, img2InterestPointsI);
            pointsCount = testMatcherInt2(img1InterestPointsI, img2InterestPointsI);
            if (i == warmupCount) {
                start = System.currentTimeMillis();
            }
        }
        long end = System.currentTimeMillis();

        logger.info("Done! Took " + ((start - end) / (numOfTries - warmupCount)) + " ms");

        assertEquals("Wrong number of matched interest points", TARGET_MATCHED_POINTS_COUNT, pointsCount);
    }

    // v1
    // ~750 ms
    public int testOriginalMatcher(List<InterestPoint> ipts1, List<InterestPoint> ipts2) {
        int matchCount = 0;

        float distance, bestDistance, secondBest;
        int descSize = 64; // TODO: use parameter
        float delta;
        float[] v1, v2;

        for (InterestPoint p1 : ipts1) {
            bestDistance = secondBest = Float.MAX_VALUE;

            ipts2Loop:
            for (InterestPoint p2 : ipts2) {

                // (NB: There is no check fo sign of laplacian in OpenSURF)
                if (p1.isSign() != p2.isSign()) {
                    continue;
                }

                // Compare descriptors (based on calculating of squared distance between two vectors)
                distance = 0;
                v1 = p1.getDescriptor();
                v2 = p2.getDescriptor();
                for (int i = 0; i < descSize; i++) {
                    delta = v1[i] - v2[i];
                    distance += delta * delta;
                    if (distance >= secondBest) {
                        continue ipts2Loop;
                    }
                }
                if (distance < bestDistance) {
                    secondBest = bestDistance;
                    bestDistance = distance;
                    //bestMatch = p2;
                }
                else { // distance < secondBest
                    secondBest = distance;
                }

            }


            // Threshold values in other implementations:
            // OpenSURF:                    0.65
            // OpenCV-2.0.0 (find_obj.cpp): 0.6
            // Orig. SURF:                  0.5
            if (bestDistance < 0.5f * secondBest) {

                // Matching point found.
                //res.put(p1, bestMatch);
                matchCount++;
            }
        }
        return matchCount;
    }

    // --------------------------------------------------
    // v2
    // 770ms
    public int testMatcherFloat(List<InterestPointFloat> ipts1, List<InterestPointFloat> ipts2) {
        int matchCount = 0;

        float distance, bestDistance, secondBest;
        int descSize = 64; // TODO: use parameter
        float delta;
        float[] v1, v2;

        for (InterestPointFloat p1 : ipts1) {
            bestDistance = secondBest = Float.MAX_VALUE;

            ipts2Loop:
            for (InterestPointFloat p2 : ipts2) {

                // (NB: There is no check fo sign of laplacian in OpenSURF)
                if (p1.isSign() != p2.isSign()) {
                    continue;
                }

                // Compare descriptors (based on calculating of squared distance between two vectors)
                distance = 0;
                v1 = p1.getDescriptor();
                v2 = p2.getDescriptor();
                for (int i = 0; i < descSize; i++) {
                    delta = v1[i] - v2[i];
                    distance += delta * delta;
                    if (distance >= secondBest) {
                        continue ipts2Loop;
                    }
                }
                if (distance < bestDistance) {
                    secondBest = bestDistance;
                    bestDistance = distance;
                    //bestMatch = p2;
                }
                else { // distance < secondBest
                    secondBest = distance;
                }

            }


            // Threshold values in other implementations:
            // OpenSURF:                    0.65
            // OpenCV-2.0.0 (find_obj.cpp): 0.6
            // Orig. SURF:                  0.5
            if (bestDistance < 0.5f * secondBest) {

                // Matching point found.
                //res.put(p1, bestMatch);
                matchCount++;
            }
        }
        return matchCount;
    }

    //---------------------------------------------------
    // v3
    // 520ms (-33%)
    public int testMatcherInt(List<InterestPointInt> ipts1, List<InterestPointInt> ipts2) {
        int matchCount = 0;

        int distance, bestDistance, secondBest;
        int descSize = 64; // TODO: use parameter
        int delta;
        int[] v1, v2;

        for (InterestPointInt p1 : ipts1) {
            bestDistance = secondBest = Integer.MAX_VALUE;

            ipts2Loop:
            for (InterestPointInt p2 : ipts2) {

                // (NB: There is no check fo sign of laplacian in OpenSURF)
                if (p1.isSign() != p2.isSign()) {
                    continue;
                }

                // Compare descriptors (based on calculating of squared distance between two vectors)
                distance = 0;
                v1 = p1.getDescriptor();
                v2 = p2.getDescriptor();
                for (int i = 0; i < descSize; i++) {
                    delta = v1[i] - v2[i];
                    distance += delta * delta;
                    if (distance >= secondBest) {
                        continue ipts2Loop;
                    }
                }
                if (distance < bestDistance) {
                    secondBest = bestDistance;
                    bestDistance = distance;
                    //bestMatch = p2;
                }
                else { // distance < secondBest
                    secondBest = distance;
                }

            }


            // Threshold values in other implementations:
            // OpenSURF:                    0.65
            // OpenCV-2.0.0 (find_obj.cpp): 0.6
            // Orig. SURF:                  0.5
            if (bestDistance < 0.5f * secondBest) {

                // Matching point found.
                //res.put(p1, bestMatch);
                matchCount++;
            }
        }
        return matchCount;
    }

    //---------------------------------------------------
    // v4
    //
    public int testMatcherInt2(List<InterestPointInt> ipts1, List<InterestPointInt> ipts2) {
        int matchCount = 0;

        int distance, bestDistance, secondBest;
        int descSize = 64; // TODO: use parameter
        int delta;
        int[] v1, v2;

        for (InterestPointInt p1 : ipts1) {
            bestDistance = secondBest = Integer.MAX_VALUE;

            ipts2Loop:
            for (InterestPointInt p2 : ipts2) {

                // (NB: There is no check fo sign of laplacian in OpenSURF)
                if (p1.isSign() != p2.isSign()) {
                    continue;
                }

                // Compare descriptors (based on calculating of squared distance between two vectors)
                distance = 0;
                v1 = p1.getDescriptor();
                v2 = p2.getDescriptor();
                for (int i = 0; i < descSize; i++) {
                    delta = v1[i] - v2[i];
                    distance += delta * delta;
                    if (distance >= secondBest) {
                        continue ipts2Loop;
                    }
                }
                if (distance < bestDistance) {
                    secondBest = bestDistance;
                    bestDistance = distance;
                    //bestMatch = p2;
                }
                else { // distance < secondBest
                    secondBest = distance;
                }

            }


            // Threshold values in other implementations:
            // OpenSURF:                    0.65
            // OpenCV-2.0.0 (find_obj.cpp): 0.6
            // Orig. SURF:                  0.5
            if (bestDistance < 0.5f * secondBest) {

                // Matching point found.
                //res.put(p1, bestMatch);
                matchCount++;
            }
        }
        return matchCount;
    }

    //---------------------------------------------------

    private String getImgFilePath(String imgName) {
        return this.getClass().getResource("/" + imgName).getFile();
    }

    private List<InterestPoint> getPOIs(String imagePath) throws IOException {
        ImagePlus image = new Opener().openImage(imagePath);
        IntegralImage intImg = new IntegralImage(image.getProcessor(), true, false);
        return IJFacade.detectAndDescribeInterestPoints(intImg, new Params());
    }

    private static class InterestPointFloat {
        boolean sign;
        float[] descriptor;

        public InterestPointFloat(boolean sign, float[] descriptor) {
            this.sign = sign;
            this.descriptor = descriptor;
        }

        public boolean isSign() {
            return sign;
        }
        public float[] getDescriptor() {
            return descriptor;
        }
    }

    private static class InterestPointInt {
        public boolean sign;
        public int[] descriptor;

        public InterestPointInt(boolean sign, int[] descriptor) {
            this.sign = sign;
            this.descriptor = descriptor;
        }

        public boolean isSign() {
            return sign;
        }
        public int[] getDescriptor() {
            return descriptor;
        }
    }

    private static class InterestPointShort {
        public boolean sign;
        public short[] descriptor;

        public InterestPointShort(boolean sign, short[] descriptor) {
            this.sign = sign;
            this.descriptor = descriptor;
        }

        public boolean isSign() {
            return sign;
        }
        public short[] getDescriptor() {
            return descriptor;
        }
    }
}
